//
//  SideBarViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 15/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case Profile = 0
    case DashBoard
    case Feedback
    case About
    case Service
//    case Geofancing
    case Notification_Setting
    case Logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class SideBarViewController: UIViewController {
let alert_view = AlertView.instanceFromNib()
    let names = ["", "Dashboard", "FeedBack", "About", "Service Provider", "Notification Setting", "Logout"]
     let image_names = ["", "dashboard", "feedback", "about", "services", "notification_setting", "logout"]
    @IBOutlet weak var tbl_side_menu: UITableView!
    var dashboardViewController: UIViewController!
    var feebackViewController: UIViewController!
    var aboutviewVontroller: UIViewController!
    var serviceproviderViewController: UIViewController!
    var profileViewController: UIViewController!
    var serviceViewController: UIViewController!
    var geofancing: UIViewController!
    var notificationSetting: UIViewController!
    
    let logoutView = MessagewithButtonView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        logoutView.btn_yes.addTarget(self, action: #selector(pressed_yes_logout), for: .touchUpInside)
        logoutView.btn_no.addTarget(self, action: #selector(pressed_no_logout), for: .touchUpInside)
        self.tbl_side_menu.register(UINib(nibName: SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SIDEBAR_CELL)
        self.tbl_side_menu.register(UINib(nibName: SUB_SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SUB_SIDEBAR_CELL)
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewControllerVC") as! ProfileViewControllerVC
                self.profileViewController = UINavigationController(rootViewController: profileViewController)
             
        
                self.dashboardViewController = storyboard.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
                self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
        
                self.feebackViewController = storyboard.instantiateViewController(withIdentifier: "FeedBackVC") as! FeedBackVC
                self.feebackViewController = UINavigationController(rootViewController: feebackViewController)
        
                self.aboutviewVontroller = storyboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
                self.aboutviewVontroller = UINavigationController(rootViewController: aboutviewVontroller)
        
        self.serviceViewController = storyboard.instantiateViewController(withIdentifier: "ServiceProvider") as! ServiceProvider
        self.serviceViewController = UINavigationController(rootViewController: serviceViewController)
        
        self.geofancing = storyboard.instantiateViewController(withIdentifier: "ServiceProvider") as! ServiceProvider
        self.geofancing = UINavigationController(rootViewController: geofancing)
        
     
            self.notificationSetting = storyboard.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
       
        self.notificationSetting = UINavigationController(rootViewController: notificationSetting)
     
        
        tbl_side_menu.delegate = self
        tbl_side_menu.dataSource = self
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        tbl_side_menu.reloadData()
    }
    
    /*
     * This way we instantiate ViewController when needed, not on SideMenu initialization (viewDidLoad), because we don't want it in memory until we go to that view
     */
    func instantiateDynamicallyCreatedViewControllers() {
        let serviceViewController = ServiceProvider()
        serviceViewController.view.backgroundColor = UIColor.orange
        self.serviceViewController = UINavigationController(rootViewController: serviceViewController)
    }
    
    /*
     * This way we deallocate ViewController after changing to another ViewController, so we don't keep it in memory all the time
     */
    func deallocateDynamicallyCreatedViewControllers() {
        self.serviceViewController = nil
    }
    
    @objc func pressed_yes_logout(){
         LogoutFromServer()
    }
    
    @objc func pressed_no_logout(){
        logoutView.removeFromSuperview()
    }
    
    func changeViewController(menu: LeftMenu) {
       // deallocateDynamicallyCreatedViewControllers()
        
        switch menu {
            
        case .Profile:
            self.revealViewController().pushFrontViewController(self.profileViewController, animated: true)

            break
        case .DashBoard:
            self.revealViewController().pushFrontViewController(self.dashboardViewController, animated: true)
          
            break
        case .About:
            self.revealViewController().pushFrontViewController(self.aboutviewVontroller, animated: true)
            break
        case .Feedback:
            self.revealViewController().pushFrontViewController(self.feebackViewController, animated: true)
            break
        
        case .Service:
           // instantiateDynamicallyCreatedViewControllers()
            self.revealViewController().pushFrontViewController(self.serviceViewController, animated: true)
            break
//        case .Geofancing:
//            instantiateDynamicallyCreatedViewControllers()
//            self.revealViewController().pushFrontViewController(self.geofancing, animated: true)
//            break
//
        case .Notification_Setting:

            self.revealViewController().pushFrontViewController(self.notificationSetting, animated: true)
            
        case .Logout:
            logoutView.frame.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            UIApplication.shared.keyWindow!.addSubview(logoutView)
            
        }
        
    }
    
    func LogoutFromServer(){
        print("Logout")
        UIApplication.shared.keyWindow!.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
     
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as! String
       
        
       //https://track.georadius.in/login_result.php?action=logoutApp&user_id=2212&user_app_id=4ccafb1053b6983e34d572d75388d920a5cb9ce8676577975d7a62cc8a3ceb81
        
        let urlString = domain_name + "/login_result.php?action=logoutApp&user_id=" + user_id + "&user_app_id=" + device_token
        
        CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //showToast(controller: self, message : "Logout", seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
            SaveLoginKey(key : "")
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDel.window?.rootViewController = loginVC
        })
        
    
    }

}
extension SideBarViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl_side_menu.dequeueReusableCell(withIdentifier: SIDEBAR_CELL) as! SideBarCell
        let sub_cell = tbl_side_menu.dequeueReusableCell(withIdentifier: SUB_SIDEBAR_CELL) as! SubSideBarCell
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let phone = UserDefaults.standard.value(forKey: PHONE) as! String
        
        if indexPath.row > 0{
            sub_cell.lbl_name.text = names[indexPath.row]
            sub_cell.img_item.image = UIImage(named: image_names[indexPath.row])
            return sub_cell
        }else{
            cell.lbl_name.text = user_name.firstUppercased
            cell.lbl_contact.text = phone
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return tbl_side_menu.frame.size.height / 4.5
        }else{
            return  tbl_side_menu.frame.size.height / 12.5
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu: menu)
            
            switch menu {
            case .Profile: break
            case .DashBoard: break
            case .About: break
            case .Feedback: break
            case .Service: break
//            case .Geofancing: break
            case .Notification_Setting: break
            case .Logout:
                break
            }
        }
    }
}
