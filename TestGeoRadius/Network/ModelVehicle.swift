//
//  ModelVehicle.swift
//  GeoTrack
//
//  Created by Georadius on 25/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation


class VehicleDetail{
    
    var registration : String!
    var speed : String!
    var location : String!
    var distance : Double!
    var status : Int!
    var latitude : Double!
    var device_id : String!
    var longitude : Double!
    var halt_time : String!
    var last_update : String!
    var ignition_status : String!
    var digital_sensor_status : String!
    var vehicle_type_id : String!
    
    init(registration: String, speed: String, location: String, distance: Double, status: Int, latitude : Double, device_id : String, longitude : Double, halt_time : String, last_update : String, ignition_status : String,  digital_sensor_status : String, vehicle_type_id : String) {
        self.registration = registration
        self.speed = speed
        self.location = location
        self.distance = distance
        self.status = status
        self.latitude = latitude
        self.device_id = device_id
        self.longitude = longitude
        self.halt_time = halt_time
        self.last_update = last_update
        self.ignition_status = ignition_status
        self.digital_sensor_status = digital_sensor_status
        self.vehicle_type_id = vehicle_type_id
        
    }
}
