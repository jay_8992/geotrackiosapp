//
//  CustomImageAnnotationView.swift
//  TestGeoRadius
//
//  Created by Georadius on 24/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation
import Mapbox

class CustomImageAnnotationView: MGLAnnotationView {
    var imageView: UIImageView!
    
    required init(reuseIdentifier: String?, image: UIImage) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.imageView = UIImageView(image:image)
        self.addSubview(self.imageView)
        self.frame = self.imageView.frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}
