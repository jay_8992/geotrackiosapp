//
//  TripDataModel.swift
//  GeoTrack
//
//  Created by Georadius on 25/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class TripVehicle{
    
    
    var speed : Double!
   // var location : String!
    var status : Int!
    var latitude : Double!
    var longitude : Double!
    var direction : Double!
    var start_date : String!
    var place : String!
    
    init(speed: Double, status: Int, latitude: Double, longitude: Double, direction: Double, start_date: String, place: String){
        self.speed = speed
        //self.location = location
        self.status = status
        self.latitude = latitude
        self.longitude = longitude
        self.direction = direction
        self.start_date = start_date
        self.place = place
    }
}
