//
//  RenewLicenseModel.swift
//  GeoTrack
//
//  Created by Georadius on 03/08/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class RenewLicenseModel{
    
    var registration_no : String!
    var device_serial : String!
    var valid_date :  String!
    var device_id : String!
   
    
    init(registration_no: String, device_serial: String, valid_date: String, device_id: String){
        self.registration_no = registration_no
        self.device_serial = device_serial
        self.valid_date = valid_date
        self.device_id = device_id
       
    }
}
