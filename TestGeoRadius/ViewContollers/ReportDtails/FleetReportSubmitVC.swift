//
//  FleetReportSubmitVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FleetReportSubmitVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    
    let alert_view = AlertView.instanceFromNib()
    
    var alert_data_registration = [""]
   var idle_time = [""]
    var moving_time = [""]
    var stopped_time = [""]
    var per_idle_time = [""]
    var per_moving_time = [""]
    var per_stopped_time = [""]
    
    var maxValueIdle = "0.0"
    var maxValueMoving = "0.0"
    var maxValueStopped = "0.0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tbl_report.register(UINib(nibName: "FleetUsesTimeCell", bundle: nil), forCellReuseIdentifier: "FleetUsesTimeCell")
      
        self.tbl_report.register(UINib(nibName: "FleetUsesReport", bundle: nil), forCellReuseIdentifier: "FleetUsesReport")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //CallDataFromServer()
        
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
        
    }
    
    
    @objc func BottomToTop(){
        if alert_data_registration.count > 1{
        let indexPath = IndexPath(row: 0, section: 0)
        self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: [String]){
        //alert_data_registration.removeAll()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        
        alert_data_registration = [""]
        idle_time = [""]
        moving_time = [""]
        stopped_time = [""]
        per_idle_time = [""]
        per_moving_time = [""]
        per_stopped_time = [""]
        
        let device_ids = device_id.joined(separator: ",")
        //let alert_type_ids = alert_type_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        
        let urlString = domain_name + Fleet_All + device_ids + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        
       
      
        CallFleetReport(urlString: urlString, completionHandler: {data, r_error, isNetwork, idle, moving, stopped in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                self.maxValueIdle = idle!
                self.maxValueMoving = moving!
                self.maxValueStopped = stopped!
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.alert_data_registration.append(GetRegistrationNumber(Vehicals: val_data))
                    self.idle_time.append(val_data["idling_time"] as! String)
                    self.moving_time.append(val_data["moving_time"] as! String)
                    self.stopped_time.append(val_data["stoppage_time"] as! String)
                    self.per_idle_time.append(val_data["per_idle_time"] as! String)
                    self.per_moving_time.append(val_data["per_moving_time"] as! String)
                    self.per_stopped_time.append(val_data["per_stoppage_time"] as! String)
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                    
                }else{
                    showToast(controller: self, message: "Please check Internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
            }
            
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
}

extension FleetReportSubmitVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
     
        return alert_data_registration.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "FleetUsesTimeCell") as! FleetUsesTimeCell
        let report_cell = tbl_report.dequeueReusableCell(withIdentifier: "FleetUsesReport") as! FleetUsesReport
        
        if indexPath.section == 0{
            cell.lbl_stopped.text = maxValueStopped + " Hrs"
            cell.lbl_moving.text = maxValueMoving + " Hrs"
            cell.lbl_idle.text = maxValueIdle + " Hrs"
            return cell
        }else{
            report_cell.lbl_idle.text = idle_time[indexPath.section] + "|" + per_idle_time[indexPath.section] + " %"
            report_cell.lbl_moving.text = moving_time[indexPath.section] + "|" + per_moving_time[indexPath.section] + " %"
            report_cell.lbl_stopped.text = stopped_time[indexPath.section] + "|" + per_stopped_time[indexPath.section] + " %"
            return report_cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 55
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderViewReportAll.instanceFromNib()
        view.lbl_registration.text = alert_data_registration[section]
        return view
    }
    
}
