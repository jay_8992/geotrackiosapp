//
//  CameraReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

protocol ViewImageData {
    func viewImage(urlString: String)
}

class CameraReportVC: UIViewController {

    @IBOutlet weak var tbl_report: UITableView!
    @IBOutlet weak var lbl_error: UILabel!
    let alert_view = AlertView.instanceFromNib()

    var location = [String]()
    var pic_id = [String]()
    var create_date = [String]()
    var ssd : CameraVC?
    
    var delegate : ViewImageData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
        self.tbl_report.register(UINib(nibName: "CameraReportCell", bundle: nil), forCellReuseIdentifier: "CameraReportCell")

        // Do any additional setup after loading the view.
    }
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: [String]){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        location.removeAll()
        pic_id.removeAll()
        create_date.removeAll()
        
        let device_ids = device_id.joined(separator: ",")
        //let alert_type_ids = alert_type_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
     
        
        let urlString = domain_name + "/vehicle_result.php?&action=photo_search&device_id="  +  device_ids + "&date_from="  +  from_date + "&date_to="  + to_date  +  "&time_picker_from="  +  from_time  +  "&time_picker_to="  +  to_time  +  "&user_name=" + user_name + "&hash_key=" + hash_key + "&data_format=1"
        
    
        
        CallTripReport(urlString: urlString, key_val: "devicepicture_data", completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.location.append(val_data["location"] as! String)
                    self.pic_id.append(val_data["devicepicture_id"] as! String)
                    self.create_date.append(val_data["create_date"] as! String)
                    
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                }else{
                    //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
               
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
        
    }
    


}

extension CameraReportVC{
    @objc func pressed_view_button(sender: UIButton){
        
        let id = self.pic_id[sender.tag]
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String

        let urlString = domain_name + "/pic_data_display.php?devicepicture_id=" + id
        delegate?.viewImage(urlString: urlString)
    }
}

extension CameraReportVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return location.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "CameraReportCell") as! CameraReportCell
        cell.lbl_location.text = self.location[indexPath.row]
        cell.lbl_speed.text = self.create_date[indexPath.row]
        cell.btn_view.tag = indexPath.row
        cell.btn_view.addTarget(self, action: #selector(pressed_view_button(sender:)), for: .touchUpInside)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 5
    }
    
}
