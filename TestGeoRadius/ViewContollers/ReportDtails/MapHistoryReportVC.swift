//
//  MapHistoryReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 23/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps


class MapHistoryReportVC: UIViewController {

    @IBOutlet weak var lbl_trips: UILabel!
    @IBOutlet weak var btn_zoom_out: UIButton!
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var speed_picker: UIPickerView!
    @IBOutlet weak var lbl_place: UILabel!
    @IBOutlet weak var lbl_start_date: UILabel!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var lbl_sum_of_time: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_Vehicle_Name: UILabel!
    @IBOutlet weak var view_detail_show: UIView!
    @IBOutlet weak var btn_play_pause: UIButton!
    @IBOutlet weak var view_lbl_name: UIView!
    @IBOutlet weak var btn_down: UIButton!
    @IBOutlet weak var btn_up: UIButton!
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var showMap: GMSMapView!
    var latitude = [Double]()
    var longitude = [Double]()
    var direction = [Double]()
    var speed_limit = [Double]()
    var start_date = [String]()
    var place = [String]()
    var ignition_status = [Int]()
    var distance : String?
    var working_hour : String?
    var VehicleName : String?
     let alert_view = AlertView.instanceFromNib()
     var allCoordinates = [CLLocationCoordinate2D]()
    let speed_x = ["1X", "2X", "3X", "4X"]

    var play = false
    var view_detail_origin : CGFloat!
    var iTemp:Int = 0
    var marker = GMSMarker()
    var rectangle = GMSPolyline()
    var timer = Timer()
    var time_interval = [1.2, 0.9, 0.6, 0.3]
    var val_vehicle_type_id : String!
    
    
    var interval = 1.2
    
    override func viewDidLoad() {
        super.viewDidLoad()
view_lbl_name.dropShadow(color: .gray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
//showMap.delegate = self
  //      moveMent.delegate = self
        speed_picker.delegate = self
        speed_picker.dataSource = self
        btn_up.addTarget(self, action: #selector(btn_up_pressed), for: .touchUpInside)
        btn_down.addTarget(self, action: #selector(btn_down_pressed), for: .touchUpInside)
        btn_play_pause.addTarget(self, action: #selector(Play_Pause_pressed), for: .touchUpInside)
        view_detail_origin = view_detail_show.frame.origin.y
        
        view_detail_show.frame.origin.y = self.view.frame.size.height
        
        btn_zoom_in.layer.cornerRadius = btn_zoom_in.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)
        
        btn_zoom_out.layer.cornerRadius = btn_zoom_out.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
    }
    
    @objc func btn_up_pressed(){
        view_detail_show.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.view_detail_show.frame.origin.y = self.view_detail_origin
        })
    }
    
    @objc func btn_down_pressed(){
        
        //view_detail_show.isHidden = true
        UIView.animate(withDuration: 0.5, animations: {
            self.view_detail_show.frame.origin.y = self.view.frame.size.height
        })
    }
    
    @objc func Play_Pause_pressed(){
        if play{
            timer.invalidate()
            btn_play_pause.setImage(UIImage(named: PLAY), for: .normal)
            play = false
        }else{
            btn_play_pause.setImage(UIImage(named: PAUSE), for: .normal)
            timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (_) in
                self.playCar()
            })
            
            RunLoop.current.add(timer, forMode: .tracking)
            play = true
        }
    }
    
    @objc func pressed_zoom_in(){
        if showMap.isHidden{
            return
        }
        let zoom_in = self.showMap.camera.zoom + 2
        let loc : CLLocation = CLLocation(latitude: latitude[iTemp], longitude: longitude[iTemp])
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
        
    }
    
    @objc func pressed_zoom_out(){
        if showMap.isHidden{
            return
        }
        let loc : CLLocation = CLLocation(latitude: latitude[iTemp], longitude: longitude[iTemp])
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    func RemoveAllThings(){
        self.showMap.clear()
        self.rectangle.map = nil
        self.timer.invalidate()
        iTemp = 0
        marker.map = nil
        self.VehicleName = "----"
        self.distance = "----"
        self.working_hour = "----"
         latitude.removeAll()
         longitude.removeAll()
         direction.removeAll()
         speed_limit.removeAll()
         start_date.removeAll()
         place.removeAll()
        ignition_status.removeAll()
        play = false
        
    }
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: String, val_vehicle_type_id : String){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
           // self.tbl_report.isHidden = true
             self.showMap.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        RemoveAllThings()
        self.val_vehicle_type_id = val_vehicle_type_id
        self.view.addSubview(alert_view)
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Track_History + device_id + "&date_from=" + from_date+from_time + "&date_to=" + to_date+to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
 
        //print("sdlflsdll \(String(describing: encodedString))")
        CallTrackResult(urlString: encodedString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let trip_data = c_data["trip_data"] as! Array<Any>
                    self.VehicleName = GetRegistrationNumber(Vehicals: c_data)
                    self.distance = c_data["sum_of_distance"] as? String
                    self.working_hour = c_data["total_working_hours"] as? String
                    
                    let trip_count = c_data["trip_count"] as! Int
                    self.lbl_trips.text = String(trip_count)
                    
                    for val in trip_data{
                        let val_data = val as! Dictionary<String, Any>
                        let latitude = val_data["latitude"] as! Double
                        
                      self.latitude.append(latitude)
                        
                        let longitude = val_data["longitude"] as! Double
                        
                      self.longitude.append(longitude)
                        let angle = Double(val_data["direction"] as! String)
                        self.direction.append(angle!)
                        let speed = Double(val_data["speed"] as! String)
                        self.speed_limit.append(speed!)
                        self.start_date.append(val_data["start_date"] as! String)
                        self.place.append(val_data["place"] as! String)
                        self.ignition_status.append(val_data["vehicle_status"] as! Int)
                    }
                            
                    self.lbl_distance.text = self.distance! + "KM"
                    self.lbl_sum_of_time.text = self.working_hour!
                    self.lbl_Vehicle_Name.text = self.VehicleName!
                    self.showMap.isHidden = false
                    break
                case _ where result > 0 :
                    self.showMap.isHidden = true
                    self.lbl_distance.text = self.distance!
                    self.lbl_sum_of_time.text = self.working_hour!
                    self.lbl_Vehicle_Name.text = self.VehicleName!
                    self.lbl_trips.text = "--"
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                print("ERROR FOUND")
            }
            
            self.alert_view.removeFromSuperview()
           // self.AddPolyLine()
            if self.latitude.count > 0{
                 self.drawPathOnMap()
            }else{
                self.showMap.isHidden = true
            }
           
        })
        
    }
}

extension MapHistoryReportVC{
    func drawPathOnMap()  {
        let path = GMSMutablePath()
        var marker = GMSMarker()
        
        
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 12)
        self.showMap!.camera = camera
        
        for val in 0..<2{
            
            if val == 0{
                let initialLocation = CLLocationCoordinate2DMake(self.latitude[0], self.longitude[0])
                marker = GMSMarker(position: initialLocation)
                marker.icon = UIImage(named: "startPoint")
                marker.map = self.showMap
            }
            if val == 1{
             
                let initialLocation = CLLocationCoordinate2DMake(self.latitude[self.latitude.count - 1], self.longitude[self.longitude.count - 1])
                marker = GMSMarker(position: initialLocation)
                marker.icon = UIImage(named: "endPoint")
                marker.map = self.showMap
            }
            
        }
        for (index, _) in self.latitude.enumerated()
        {
            if self.ignition_status[index] == 1{
                let initialLocation = CLLocationCoordinate2DMake(self.latitude[index], self.longitude[index])
                marker = GMSMarker(position: initialLocation)
                marker.icon = UIImage(named: "ignition_mark")
                marker.map = self.showMap
            }
            
            
            
        }
        

        let inialLat:Double = self.latitude[0]
        let inialLong:Double = self.longitude[0]

     
        for (index, _) in self.latitude.enumerated()
        {
            path.add(CLLocationCoordinate2DMake(latitude[index], longitude[index]))
            
           
        }
        //set poly line on mapview
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0
        for (index, _) in self.latitude.enumerated()
        {
        if speed_limit[index] > 60{
            rectangle.strokeColor = .red
        }else{
            rectangle.strokeColor = Moving_Color
        }
        }
        marker.map = showMap
        rectangle.map = showMap

        //Zoom map with path area
        let loc : CLLocation = CLLocation(latitude: inialLat, longitude: inialLong)
        updateMapFrame(newLocation: loc, zoom: 18.0)

//        timer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: true, block: { (_) in
//            self.playCar()
//        })
//
//        RunLoop.current.add(timer, forMode: .tracking)
    }

    func playCar()
    {
        if iTemp <= (self.latitude.count - 1 )
        {
          
            UIView.animate(withDuration: 0.3, animations: {
                self.marker.groundAnchor = CGPoint(x: 0.5, y: 0.3)
                self.marker.rotation = self.direction[self.iTemp]
                self.showMap.animate(toBearing: 0)
            }, completion: { _ in
                self.MoveTheCar() 
            })
        }
    }
    
    func MoveTheCar(){
        self.lbl_speed.text = String(self.speed_limit[iTemp]) + "KMPH"
        self.lbl_start_date.text = self.start_date[iTemp]
        self.lbl_place.text = self.place[iTemp]
        
        let loc : CLLocation = CLLocation(latitude: latitude[iTemp], longitude: longitude[iTemp])
        
        updateMapFrame(newLocation: loc, zoom: self.showMap.camera.zoom)
        marker.position = CLLocationCoordinate2DMake(latitude[iTemp], longitude[iTemp])
        
        var vehicle_name = "car"
        if self.val_vehicle_type_id == "56"{
            vehicle_name = "police"
        }else if self.val_vehicle_type_id == "1"{
            vehicle_name = "car"
        }else if self.val_vehicle_type_id == "2"{
            vehicle_name = "truck"
        }else if self.val_vehicle_type_id == "3"{
            vehicle_name = "bus"
        }else if self.val_vehicle_type_id == "4"{
            vehicle_name = "van"
        }else if self.val_vehicle_type_id == "5"{
            vehicle_name = "car"
        }else if self.val_vehicle_type_id == "6"{
            vehicle_name = "bike"
        }else if self.val_vehicle_type_id == "7"{
            vehicle_name = "scooty"
        }else{
            vehicle_name = "car"
        }
        
        marker.icon = UIImage(named: vehicle_name + "_green")
        marker.setIconSize(scaledToSize: .init(width: 20, height: 40))
        
        marker.map = showMap
        
        // Timer close
        if iTemp == (self.latitude.count - 1)
        {
            // timer close
            timer.invalidate()
            //buttonPlay.isEnabled = true
            iTemp = 0
        }
        iTemp += 1
    }
    
    

    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.showMap.animate(to: camera)
    }

}
extension MapHistoryReportVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return speed_x.count
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return speed_x[row]
//    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        interval = time_interval[row]
        timer.invalidate()
        btn_play_pause.setImage(UIImage(named: "play"), for: .normal)
        play = false
        self.Play_Pause_pressed()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.speed_picker.frame.size.width, height: self.speed_picker.frame.size.height - 10))
        let label_speed: UILabel = UILabel(frame: CGRect(x:0, y: 0, width: pickerLabel.frame.size.width, height: pickerLabel.frame.size.height))
        label_speed.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        label_speed.text = speed_x[row]
        label_speed.textAlignment = .center
        pickerLabel.addSubview(label_speed)
        
        return pickerLabel
    }
    
}

    
//
// func AddPolyLine(){
//
//
//    let path = GMSMutablePath()
//
//    for (index, _) in self.latitude.enumerated(){
//        path.add(CLLocationCoordinate2DMake(self.latitude[index], self.longitude[index]))
//    }
//    let polyline = GMSPolyline(path: path)
//    polyline.strokeWidth = 5.0
//    polyline.geodesic = true
//    polyline.strokeColor = .blue
//    polyline.geodesic = false
//
//    polyline.map = self.showMap
//
//
//    oldCoordinates = CLLocationCoordinate2DMake(self.latitude[0], self.longitude[0])
//
//   // CATransaction.begin()
//    //CATransaction.setValue(Int(12.0), forKey: kCATransactionAnimationDuration)
//    //CATransaction.setCompletionBlock({
//        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
//        self.cmera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 16)
//        self.showMap!.camera = self.cmera
//        let initialLocation = CLLocationCoordinate2DMake(self.latitude[0], self.longitude[0])
//        let marker = GMSMarker(position: initialLocation)
//        marker.map = self.showMap
//
////        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.animateLine), userInfo: nil, repeats: true)
//
//    //})
//
////CATransaction.commit()
//
//   CreateLoopForAnimate()
//
//}
//
//    func CreateLoopForAnimate(){
////        self.timer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true, block: { (_) in
////            self.animateLine()
////
////        })
////        RunLoop.current.add(self.timer, forMode: .common)
//         self.animateLine()
//    }
//
//     func animateLine(){
//
//      //  if i <= self.latitude.count - 1{
//           // self.SetCameraPosition()
//
//            let camera = GMSCameraPosition.camera(withLatitude: self.latitude[self.i], longitude:  self.longitude[self.i], zoom: 15)
//            self.showMap.animate(to: camera)
//
//            if self.i > 0{
//
//                self.london.map = nil
//
//            }
//            self.london.position = CLLocationCoordinate2DMake(self.latitude[self.i], self.longitude[self.i])
//            self.london.icon = UIImage(named: MOVING_CAR)
//
//            self.london.map = self.showMap
//
////            if i == self.latitude.count - 1{
////                timer.invalidate()
////                i = 0
////            }
////            i += 1
////        }
//
//
//           self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerTriggerd), userInfo: nil, repeats: true)
//
//
//
//    }
//
////    func SetCameraPosition(){
////        let camera = GMSCameraPosition.camera(withLatitude: self.latitude[self.i], longitude:  self.longitude[self.i], zoom: 15)
////        self.showMap.animate(to: camera)
////    }
//
//    @objc func timerTriggerd(){
//        if i < self.latitude.count{
//            let newCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.latitude[i], self.longitude[i])
//            self.moveMent.arCarMovement(marker: london, oldCoordinate: self.oldCoordinates, newCoordinate: newCoordinate, mapView: self.showMap, bearing: 0)
//            self.oldCoordinates = newCoordinate
//            self.i = self.i + 1
//
//        }else{
//            self.timer.invalidate()
//            //self.timer = nil
//        }
//    }
//
//}
//
//extension MapHistoryReportVC: GMSMapViewDelegate{
//
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//
//
//    }
//
//}
//
//extension MapHistoryReportVC: ARCarMovementDelegate{
//    func arCarMovementMoved(_ marker: GMSMarker) {
//        london = marker
//        london.map = self.showMap
//        let updateCamera : GMSCameraUpdate = GMSCameraUpdate.setTarget(london.position, zoom: 15)
//        self.showMap.animate(with: updateCamera)
//    }
//
//
//}


