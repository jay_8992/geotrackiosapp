//
//  VehicleDistanceReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleDistanceReportVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    
    let alert_view = AlertView.instanceFromNib()
    var start_date = [String]()
    var end_date = [String]()
   var device_data = [NSArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        self.tbl_report.register(UINib(nibName: "VehicleDistanceCell", bundle: nil), forCellReuseIdentifier: "VehicleDistanceCell")
        self.tbl_report.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")

     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
    }
    
    
    @objc func BottomToTop(){
        if start_date.count > 0{
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: [String], group_hour: String){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        
        start_date.removeAll()
        end_date.removeAll()
        device_data.removeAll()
        
        let device_ids = device_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let from_group_hour = group_hour
        let date_time = from_group_hour.components(separatedBy: " ")
        let group_h = date_time[2]
        
        let urlString = domain_name + "/report_vehicle_distance_result.php?data_format=json&device_id="  +  device_ids + "&date_from="  +  from_date + "&date_to="  + to_date  +  "&time_picker_from="  +  from_time  +  "&time_picker_to="  +  to_time  +  "&group_hour="  +  group_h +  "&group_mode=1&distance_covered_unit=0&method_type=2&user_name=" + user_name + "&hash_key=" + hash_key
        
        
        CallTripReport(urlString: urlString, key_val: "vehicledistance_data", completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.start_date.append(val_data["start_date"] as! String)
                    self.end_date.append(val_data["end_date"] as! String)
                    self.device_data.append(val_data["device_data"] as! NSArray)
                }
                
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                }else{
                   // showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }

}
extension VehicleDistanceReportVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return start_date.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if device_data[section].count < 1{
            return 1
        }else{
            return device_data[section].count

        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "VehicleDistanceCell") as! VehicleDistanceCell
        let no_cell = tbl_report.dequeueReusableCell(withIdentifier: "NoDataCell") as! NoDataCell
        if device_data[indexPath.section].count < 1{
            return no_cell
        }else{
            let vehicle_distance = device_data[indexPath.section][indexPath.row] as! Dictionary<String, Any>
            cell.lbl_distance.text = vehicle_distance["total_distance"] as? String
            cell.lbl_ang_speed.text = vehicle_distance["speed"] as? String
            cell.lbl_registration.text = vehicle_distance["registration_no"] as? String
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return tbl_report.frame.size.height / 4
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 75
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = VehicleDIstanceHeaderView.instanceFromNib()
        view.lbl_start_date.text = start_date[section]
        view.lbl_end_date.text = end_date[section]
        return view
    }
}
