//
//  FuelReportSubmitVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FuelReportSubmitVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    let alert_view = AlertView.instanceFromNib()

    var fuel_level = [String]()
    var distance = [String]()
    var date_time = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tbl_report.delegate = self
        tbl_report.dataSource = self
        
        self.tbl_report.register(UINib(nibName: "FuelCell", bundle: nil), forCellReuseIdentifier: "FuelCell")
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //CallDataFromServer()
        
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
    }
    
    
    
    @objc func BottomToTop(){
        if date_time.count > 0{
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: String){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        fuel_level.removeAll()
        distance.removeAll()
        date_time.removeAll()
      
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String

        let urlString = domain_name + Fuel_All + device_id + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        print("sdfsdkj \(urlString)")
        CallFuelReport(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.date_time.append(val_data["log_date_start"] as! String)
                    self.fuel_level.append(val_data["fuel_level"] as! String)
                    let dist = String(val_data["distance"] as! Double)
                    self.distance.append(dist)
                }
                
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                
                if data != nil{
                    self.lbl_error.text = "Something went wrong."
                    self.tbl_report.isHidden = true
                }else{
                    showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
               
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
}


extension FuelReportSubmitVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return date_time.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "FuelCell") as! FuelCell
        cell.lbl_fuel.attributedText = FixBoldBetweenText(firstString: fuel_level[indexPath.row] + "%", boldFontName: " on ", lastString: date_time[indexPath.row] + " Hrs")
        cell.lbl_distance.text = distance[indexPath.row] + " Km"
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 6
    }
    
}
