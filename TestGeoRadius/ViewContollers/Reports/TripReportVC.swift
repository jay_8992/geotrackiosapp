//
//  TripReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class TripReportVC: UIViewController {
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var view_table: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var view_date_time: UIView!
    @IBOutlet weak var txt_from_date_time: UITextField!
    @IBOutlet weak var txt_to_date_time: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var view_trip_report: UIView!
    @IBOutlet weak var tbl_sort: UITableView!
    @IBOutlet weak var txt_distance_filter: UITextField!
    @IBOutlet weak var view_date_tine_perent: UIView!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var btn_filter: UIButton!
    
    var report_data_controller : TripReportSubmitVC?

    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var isFromDate = true
    var alert_report_origin : CGFloat!
    var device_id_val : String!
    var alert_view = AlertView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        self.tbl_sort.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_trip_report.frame.origin.y
        
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        txt_to_date_time.text = TodayToDate()
        
        txt_from_date_time.text = TodayFromDate()
        
        SetReportView(viewName: view_trip_report)
        
        SetOrigin()
        
    }
    
    func SetOrigin(){
        
        view_height = view_date_time.frame.size.height
        
        self.view_date_time.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_date_time.frame.origin.y + 30.0
    }
    
    @IBAction func back_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_tine_perent.isHidden = true
    }
    
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_trip_report.isHidden || view_trip_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        
//        if view_trip_report.frame.origin.y == self.alert_report_origin{
//
//            UIView.animate(withDuration: 0.5, animations: {
//                self.view_trip_report.frame.origin.y = self.header_view.frame.size.height + 30
//                //self.view_alerts_reports.isHidden = false
//            })
//        }else{
        
            UIView.animate(withDuration: 0.5, animations: {
                self.view_trip_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        //}
    }
    
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_trip_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_trip_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_trip_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    
    @IBAction func day_select(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date_time.text = TodayToDate()
            txt_from_date_time.text = TodayFromDate()
            if !view_date_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_time.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date_time.text = YesterdayFromDate()
            txt_to_date_time.text = YesterdayToDate()
            
            if !view_date_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_time.isHidden = true
            }
            break
        case 2:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_date_time.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_date_time.isHidden = false
            view_trip_report.isHidden = true
            break
        default:
            break;
        }
    }
    
    
    @IBAction func tap_from_date_time(_ sender: Any) {
        txt_from_date_time.resignFirstResponder()
        isFromDate = true
        view_date_tine_perent.isHidden = false
    }
    
    @IBAction func tap_to_date_time(_ sender: Any) {
        txt_to_date_time.resignFirstResponder()
        isFromDate = false
        view_date_tine_perent.isHidden = false
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_sort.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
                CallVehicleFromServer()
                view_table.isHidden = false
                UIView.animate(withDuration: 0.8, animations: {
                    self.view_table.frame.origin.y = 20
                })
    }
 
    
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date_time.text = dateFormatter.string(from: sender.date)
           
        }else{
            txt_to_date_time.text = dateFormatter.string(from: sender.date)
           
        }
        
        
    }
    
    @objc func submit_pressed(){
        
        if txt_to_date_time.text! <  txt_from_date_time.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        if txt_to_date_time.text! ==  txt_from_date_time.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }

        
        if txt_to_date_time.text!.count < 1 || txt_from_date_time.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        if txt_select_vehicle.text!.count < 1{
            showToast(controller: self, message: "Please Select Vehicle", seconds: 0.6)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date_time.text!)
        let to_time = GetToTime(time : txt_to_date_time.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date_time.text!)
        let from_time = GetFromTime(time: txt_from_date_time.text!)

        UIView.animate(withDuration: 0.5, animations: {
            self.view_trip_report.frame.origin.y = self.header_view.frame.size.height + 30
            //self.view_alerts_reports.isHidden = false
        })
        view_trip_report.isHidden = false
         btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        guard let locationController = children.first as? TripReportSubmitVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController

        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: device_id_val, distance: txt_distance_filter.text!)
    }
    
}

extension TripReportVC{
    func CallVehicleFromServer(){
     
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                }
                self.filterdData = self.vehicle_data
                self.tbl_sort.delegate = self
                self.tbl_sort.dataSource = self
                self.tbl_sort.reloadData()
            }else{
               // showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
}

extension TripReportVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        cell.lbl_item_name.text = filterdData[indexPath.row]
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        txt_select_vehicle.text = filterdData[indexPath.row]
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
        })
        self.device_id_val = device_id[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_sort.frame.size.height / 8
    }
    
    
}
