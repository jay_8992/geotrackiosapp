//
//  ForgotPassword.swift
//  GeoTrack
//
//  Created by Georadius on 20/09/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    
    @IBOutlet weak var btn_submit: UIButton!
    
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var view_login_card: UIView!
    @IBOutlet weak var lbl_main_error: UILabel!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    
        let alert_view = AlertView.instanceFromNib()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetTextFieldLeftSide(imageName : "email", txt_field: txt_email)
        
        SetTextFieldLeftSide(imageName : "driver_mobile", txt_field: txt_phone)
        img_icon.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        view_login_card.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        Set_Layer()
    }
    
    @IBAction func pressed_back(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func pressed_submit(_ sender: Any) {
        
        if txt_email.text!.count < 1 && txt_phone.text!.count < 1{
        lbl_main_error.isHidden = false
            return
        }
        
        lbl_main_error.isHidden = true
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let urlString = domain_name + Forgot_Password + "action=change_password&data_format=1&phone=" + txt_phone.text! + "&email=" + txt_email.text!
   
        CallForgotPasswordAPI(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{

                showToast(controller: self, message : data!, seconds: 2.0)
                
            }else{
                
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }
    
    
    func Set_Layer(){
        Set_Card_View(Card: view_login_card)
        Set_Login_Radius(Button: btn_submit)
    }


}
