//
//  UserAddVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UserAddVC: UIViewController {

    @IBOutlet weak var btn_add_user: UIButton!
    @IBOutlet weak var txt_company_name: UITextField!
    @IBOutlet weak var txt_user_address: UITextField!
    @IBOutlet weak var txt_confirm_password: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var view_header: UIView!
    
    @IBOutlet weak var btn_back: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_add_user.addTarget(self, action: #selector(pressed_submit), for: .touchUpInside)
        btn_back.addTarget(self, action: #selector(btn_back_pessed), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SetTextFields()
    }
  
    @objc func btn_back_pessed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func SetTextFields(){
        SetTextFieldLeftSide(imageName: "username", txt_field: txt_username)
        SetTextFieldLeftSide(imageName: "company_name", txt_field: txt_company_name)
        SetTextFieldLeftSide(imageName: "address", txt_field: txt_user_address)
        SetTextFieldLeftSide(imageName: "password", txt_field: txt_password)
        SetTextFieldLeftSide(imageName: "password", txt_field: txt_confirm_password)
        SetTextFieldLeftSide(imageName: "phone", txt_field: txt_phone)
    }
    
    func CallAPIToUploadData(){
        
        if txt_phone.text!.count < 1 || txt_username.text!.count < 1 || txt_password.text!.count < 1 || txt_confirm_password.text!.count < 1 || txt_user_address.text!.count < 1 || txt_company_name.text!.count < 1{
            showToast(controller: self, message: "Fields Can't be Empty", seconds: 1.5)
            return
        }
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String

        
        let urlString1 =  domain_name + "/user_result.php?action=add&username=" + txt_username.text! +  "&password=" + txt_password.text! + "&email=" + txt_username.text! + "&name=" + txt_username.text! + "&address=" + txt_user_address.text!
        
        let urlString2 = urlString1 + "&company_name=" + txt_company_name.text! + "&user_type=" + user_id + "&group_type="
        
        let urlString =  urlString2 + group_id +  "&phone=" + txt_phone.text! +  "&billing_contact=0&data_format=1&user_name=" + user_name +  "&hash_key=" + hash_key
        
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
       // print("sdfkdkfjdk \(String(describing: encodedString))")
        
        CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                // print("kjkjkjk \(data!)")
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                //print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
        })
        
        
    }

}

extension UserAddVC {
    
    @objc func pressed_submit(){
        CallAPIToUploadData()
    }
}
