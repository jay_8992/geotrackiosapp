//
//  ResetPassword.swift
//  GeoTrack
//
//  Created by Georadius on 21/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ResetPassword: UIViewController {

    @IBOutlet weak var lbl_password_match_error: UILabel!
    @IBOutlet weak var lbl_empty_error: UILabel!
    @IBOutlet weak var btn_re_enter: UIButton!
    @IBOutlet weak var btn_done: UIButton!
    @IBOutlet weak var txt_confirm_password: UITextField!
    @IBOutlet weak var txt_new_password: UITextField!
    @IBOutlet weak var btn_cross: UIButton!
    @IBOutlet weak var sub_view_reset: UIView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_back: UIButton!
     let alert_view = AlertView.instanceFromNib()
    var user_id : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        sub_view_reset.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.btn_done.addTarget(self, action: #selector(pressed_done_btn), for: .touchUpInside)
        self.btn_back.addTarget(self, action: #selector(pressed_btn_back), for: .touchUpInside)
        self.btn_re_enter.addTarget(self, action: #selector(pressed_btn_re_enter), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    
  
    
    @objc func pressed_btn_back(){
        self.navigationController?.popViewController(animated: true)
    }

    @objc func pressed_btn_re_enter(){
        lbl_empty_error.isHidden = true
            txt_new_password.text = ""
            txt_confirm_password.text = ""
    }


    @objc func pressed_done_btn(){
          if txt_new_password.text!.count < 1 || txt_confirm_password.text!.count < 1{
                    lbl_empty_error.isHidden = false
                    lbl_password_match_error.isHidden = true
                    return
                }
                
                if txt_new_password.text != txt_confirm_password.text{
                    lbl_password_match_error.isHidden = false
                     lbl_empty_error.isHidden = true
                    return
                }
                lbl_password_match_error.isHidden = true
                lbl_empty_error.isHidden = true

                let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
                let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
                let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
                //let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
                
                let urlString = domain_name + Reset_Password + "user_name=" + user_name + "&" + "hash_key=" + hash_key + "&" + "user_id=" + user_id + "&" + "password=" + txt_new_password.text!
                
                alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.view.addSubview(alert_view)
               // print("ldklskd \(urlString)")
                CallForgotPasswordAPI(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                        if isNetwork && data != nil{
        //print("sdfkllsdk")
                            showToast(controller: self, message : data!, seconds: 2.0)
                            
                        }else{
                           // print("sdfkllsdk \(isNetwork) \(String(describing: data))")
                            showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                            print("ERROR FOUND")
                        }
                        if r_error != nil{
                            showToast(controller: self, message : data!, seconds: 2.0)
                        }
                        
                        self.alert_view.removeFromSuperview()
                        
                    })
                
                
    }
    
}
