//
//  DashboardVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 28/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit



class DashboardVC: UIViewController {
    
    @IBOutlet weak var view_alert_c: UIView!
    @IBOutlet weak var segment_set: UISegmentedControl!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var collection_item: UICollectionView!
    @IBOutlet weak var Map_View: UIView!
    @IBOutlet weak var Dashboard_View: UIView!
    @IBOutlet weak var Live_View: UIView!
    var timer = Timer()
    let alert_view = AlertView.instanceFromNib()
    var liveStatus : SuccessVC?
    var mapStatus : MainMapVC?
    var all_vehicals : Array<Any> = []
    var moving_vehicals : Array<Any> = []
    var idle_vehicals : Array<Any> = []
    var stopped_vehicals : Array<Any> = []
    var noData_vehicals : Array<Any> = []
    var moving = 00
    var stopped = 00
    var unreach = 00
    var idle = 00
    var selectedPath = 4
    
    var is_queue = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let isNotification = UserDefaults.standard.value(forKey: "IsNotification") as! Bool
        
        if isNotification{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            //self.navigationController?.pushViewController(vc, animated: true)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        collection_item.delegate = self
        collection_item.dataSource = self
       // collection_item.applyGradient(colours: [.yellow, .blue, .red], locations: [0.0, 0.5, 1.0])
        SideMenu()
        view_alert_c.isHidden = true
        is_queue = true
        // print("sdkslklsdks-------------------------------------------------------------------")
        alert_view.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.size.height)
        self.view.addSubview(alert_view)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        CallTrakingDataFromServer()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        switch segment_set.selectedSegmentIndex {
        case 1:
            
            guard let locationController = children[1] as? SuccessVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
            }
            liveStatus = locationController
            liveStatus?.StartThread(isStart: true, vehic: self.all_vehicals)
            break
        default:
            print("No index")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        is_queue = false
        // self.timer.invalidate()
    }
    
    func SetHomePageOnDashboard(){
        let indexPage = UserDefaults.standard.value(forKey: "select_home_page") as! Int
        if indexPage == 2{
            
            SettingPage(selectedSegmentIndex: indexPage)
        }else if indexPage == 1{
            
            SettingPage(selectedSegmentIndex: indexPage)
        }else{
            
            SettingPage(selectedSegmentIndex: indexPage)
        }
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "liveBoard"{
            guard let locationController = children[1] as? SuccessVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
            }
            liveStatus = locationController
            liveStatus?.CheckIsHidden(isHidden_V: true)
        }
        
        if identifier == "mapBoard"{
            
            guard let MaplocationController = children[2] as? MainMapVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
            }
            mapStatus = MaplocationController
            mapStatus?.CheckIsHidden(isHidden_V: true)
            
        }
    }
    
    func SetAllDataToTable(vehicle_data: Array<Any>){
        
        for val in vehicle_data{
            
            let val_data = val as! Dictionary<String, Any>
            let status = val_data["device_status"] as! Int
            if status == 2{
                moving_vehicals.append(val_data)
            }
            if status == 1{
                idle_vehicals.append(val_data)
            }
            
            if status == 0{
                stopped_vehicals.append(val_data)
            }
            
            if status == 4{
                noData_vehicals.append(val_data)
            }
        }
        
    }
    
    
    func CallTrakingDataFromServer(){
        //self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
        
        print("Tracking URL DashBoard")
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    
                    self.moving = c_data["moving_count"] as! Int
                    self.stopped =  c_data["stopped_count"] as! Int
                    self.unreach =  c_data["unreachable_count"] as! Int
                    self.idle =  c_data["idling_count"] as! Int
                    self.all_vehicals = (c_data["vehicles"] as! Array<Any>)
                    self.collection_item.reloadData()
                    self.SetAllDataToTable(vehicle_data: self.all_vehicals)
              
                    // Broadcast Data by Notification
                    NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                    
                break
        
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }
            
            else{
                if NetworkAvailability.isConnectedToNetwork() {
                    print("Internet connection available")
                }
                else{
                    showToast(controller: self, message: "Please Check Internet Connection", seconds: 1.5)
                }
                
                //showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                print("ERROR FOUND")
            }
            if self.is_queue{
                DispatchQueue.main.asyncAfter(deadline: .now() + 15.0) {
                    // print("Timer fired! ---------")
                    [weak self] in
                    if self?.is_queue != nil && (self?.is_queue)!{
                        self!.CallTrakingDataFromServer()
                    }
                }
            }
          
            self.alert_view.removeFromSuperview()
        })
    }
    
    @objc func RepeatFunc(){
        CallTrakingDataFromServer()
    }
    
    @objc func GetTrackingData(notification: Notification){
        let c_data = notification.userInfo
        self.moving = c_data!["moving_count"] as! Int
        self.stopped =  c_data!["stopped_count"] as! Int
        self.unreach =  c_data!["unreachable_count"] as! Int
        self.idle =  c_data!["idling_count"] as! Int
        
        self.collection_item.reloadData()
        
        
        
    }
    
    @IBAction func pressed_live_track(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func pressed_segment(_ sender: UISegmentedControl) {
        
        SettingPage(selectedSegmentIndex: sender.selectedSegmentIndex)
    }
    
    @IBAction func pressed_notification(_ sender: Any) {
        is_queue = false
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func SettingPage(selectedSegmentIndex : Int){
        if selectedSegmentIndex == 0{
            is_queue = true
            CallTrakingDataFromServer()
            self.performSegue(withIdentifier: "liveBoard", sender: self)
            self.Dashboard_View.isHidden = false
            self.Live_View.isHidden = true
            self.Map_View.isHidden = true
        }
        if selectedSegmentIndex == 1{
            guard let locationController = children[1] as? SuccessVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
            }
            liveStatus = locationController
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 0.0)
            }, completion: { _ in
                self.liveStatus?.StartThread(isStart: true, vehic: self.all_vehicals)
                
            })
            
            self.Dashboard_View.isHidden = true
            self.Live_View.isHidden = false
            self.Map_View.isHidden = true
            StopAllThreads()
            is_queue = false
        }
        if selectedSegmentIndex == 2{
            
            self.is_queue = false
            
            guard let MaplocationController = children[2] as? MainMapVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
            }
            mapStatus = MaplocationController
            UIView.animate(withDuration: 0.0, animations: {
                
                switch (self.selectedPath){
                case 0 :
                    UIView.animate(withDuration: 0.0, animations: {
                        showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        self.mapStatus?.Check(Moving: true, Idle: false, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.moving_vehicals)
                        
                    })
                    
                    break
                case 1 :
                    UIView.animate(withDuration: 0.0, animations: {
                        showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        self.mapStatus?.Check(Moving: false, Idle: true, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.idle_vehicals)
                        
                        
                    })
                    break
                case 2 :
                    UIView.animate(withDuration: 0.0, animations: {
                        showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        if self.all_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.all_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: self.stopped_vehicals)
                        }
                        
                    })
                    break
                case 3 :
                    
                    UIView.animate(withDuration: 0.0, animations: {
                        showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        
                        if self.noData_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.noData_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: self.noData_vehicals)
                        }
                    })
                    
                    break
                case 4 :
                    
                    UIView.animate(withDuration: 0.0, animations: {
                        showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        
                        if self.all_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.all_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: self.all_vehicals)
                        }
                        
                    })
                    
                    break
                    
                default:
                    print("No")
                }
                
                StopAllThreads()
                self.Dashboard_View.isHidden = true
                self.Live_View.isHidden = true
                self.Map_View.isHidden = false
            }, completion: { _ in
                self.performSegue(withIdentifier: "liveBoard", sender: self)
                
            })
            
        }
    }
    
}

extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collection_item.frame.size.width / 6.0, height: self.collection_item.frame.size.width / 6.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return self.collection_item.frame.size.height / 6.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.collection_item.frame.size.height / 6.5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection_item.dequeueReusableCell(withReuseIdentifier: "CollectionItems", for: indexPath) as! CollectionItems
        
        cell.layer.cornerRadius = cell.frame.size.height / 2
        
        if self.selectedPath == indexPath.item{
            cell.layer.borderWidth = 3
            
        }else{
            cell.layer.borderWidth = 0
        }
        
        
        if indexPath.item == 0{
            cell.backgroundColor = Moving_Color
            cell.lbl_name.textColor = UIColor.white
            cell.lbl_count.textColor = UIColor.white
            cell.lbl_name.text = MOVING
            cell.lbl_count.text = String(self.moving)
            
        }
        if indexPath.item == 1{
            cell.backgroundColor = Global_Yellow_Color
            cell.lbl_name.textColor = UIColor.white
            cell.lbl_count.textColor = UIColor.white
            cell.lbl_name.text = IDLE
            cell.lbl_count.text = String(self.idle)
           
        }
        if indexPath.item == 2{
            cell.backgroundColor = Stopped_Color
            cell.lbl_name.textColor = UIColor.white
            cell.lbl_count.textColor = UIColor.white
            cell.lbl_name.text = STOPPED
            cell.lbl_count.text = String(self.stopped)
           
        }
        if indexPath.item == 3{
            cell.backgroundColor = Unreach_Color
            cell.lbl_name.textColor = UIColor.white
            cell.lbl_count.textColor = UIColor.white
            cell.lbl_name.text = UNREACH
            cell.lbl_count.text = String(self.unreach)
          
        }
        if indexPath.item == 4{
            cell.backgroundColor = Global_Blue_Color
            cell.lbl_name.textColor = UIColor.white
            cell.lbl_count.textColor = UIColor.white
            cell.lbl_name.text = ALL
            let all_count = moving + idle +  stopped + unreach
            cell.lbl_count.text = String(all_count)
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        StopAllThreads()
        is_queue = false
        selectedPath = indexPath.item
        collection_item.reloadData()

        segment_set.selectedSegmentIndex = 1
        self.Dashboard_View.isHidden = true
        self.Live_View.isHidden = false
        self.Map_View.isHidden = true
        
        
        guard let locationController = children[1] as? SuccessVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
        }
        liveStatus = locationController
        //
        guard let MaplocationController = children[2] as? MainMapVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
        }
        
        mapStatus = MaplocationController
       
        if indexPath.item == 0{
            
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: true, Idle: false, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
                
            })
            
            
        }
        if indexPath.item == 1{
            
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: true, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        }
        
        if indexPath.item == 2{
            
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
                
            })
            
        }
        
        if indexPath.item == 3{
            
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        }
        
        if indexPath.item == 4{
            
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: self.all_vehicals)
            })
        }
    }
    
}


extension DashboardVC {
    func SideMenu(){
        StopAllThreads()
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
}

