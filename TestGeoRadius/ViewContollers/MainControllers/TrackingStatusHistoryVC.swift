//
//  TrackingStatusHistoryVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 07/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import Mapbox
import UIKit.UIGestureRecognizerSubclass


// MARK: - State

private enum State {
    case closed
    case open
}


extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class TrackingStatusHistoryVC: UIViewController {

    @IBOutlet weak var lbl_trip: UILabel!
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var btn_zoom_out: UIButton!
    @IBOutlet weak var speed_picker: UIPickerView!
    @IBOutlet weak var btn_play_pause: UIButton!
    @IBOutlet weak var view_show_history: UIView!
    @IBOutlet weak var map_view: MGLMapView!
    @IBOutlet weak var view_all_detail: UIView!
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_update_time: UILabel!
    @IBOutlet weak var lbl_update_location: UILabel!
    @IBOutlet weak var lbl_update_distance: UILabel!
    @IBOutlet weak var view_show_date_submit: UIView!
    @IBOutlet weak var txt_start_date: UITextField!
    @IBOutlet weak var view_select_date_time: UIView!
    
    @IBOutlet weak var btn_date_done: UIButton!
    @IBOutlet weak var sub_view_date: UIView!
    @IBOutlet weak var btn_submit_date_time: UIButton!
    @IBOutlet weak var txt_end_date: UITextField!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_vehicle_name: UILabel!
    @IBOutlet weak var showMap: GMSMapView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var tbl_data: UITableView!
    private var bottomConstraint = NSLayoutConstraint()
    private let popupOffset: CGFloat = 265
    var device_id : String?
    let alert_view = AlertView.instanceFromNib()
    var latitude = [Double]()
    var longitude = [Double]()
    var speed_limit = [0.0]
    var direction = [Double]()
    var ignition_status = [Int]()

    var start_date = ["-----"]
    var place = ["-----"]
   
    var rasterLayer: MGLRasterStyleLayer?
    var timer: Timer?
    var polylineSource: MGLShapeSource?
    var currentIndex = 1
    var allCoordinates = [CLLocationCoordinate2D]()
    var mapView : MGLMapView!
    var date_cell_height : CGFloat = 0.0
    var submit_cell_height : CGFloat = 0.0
    var vehicle_cell_height : CGFloat = 0.0
    var play = false
    var isFromDate = false
    var view_detail_origin : CGFloat!
    var iTemp:Int = 0
    var marker = GMSMarker()
    var rectangle_green = GMSPolyline()
    var rectangle_red = GMSPolyline()
    var g_timer = Timer()
    var time_interval = [1.2, 0.9, 0.6, 0.3]
    var to_date : String!
    var from_date : String!
    var to_time : String!
    var from_time : String!
    var vehicle_type_id : String!
    let speed_x = ["1X", "2X", "3X", "4X"]
    var interval = 0.3
    var distance = "-----"
    var working_hour = "-----"
   var picker_count = 0
    var view_date_time_perent_height : CGFloat!
    var VehicleName = "-----"
    @IBOutlet weak var view_date_and_time_perent: UIView!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    var data : [TripVehicle] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        speed_picker.delegate = self
        speed_picker.dataSource = self
        
        view_show_history.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view_show_history.layer.shadowColor = UIColor.black.cgColor
        view_show_history.layer.shadowOpacity = 0.1
        view_show_history.layer.shadowRadius = 10
        view_show_history.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 3), radius: 5, scale: true)
     // tbl_data.delegate = self
      //  tbl_data.dataSource = self
        
//        let indexPath = IndexPath(row: 2, section: 0)
//        let cell = tbl_data.cellForRow(at: indexPath) as! Vehicle_Detail_Cell
//        cell.speed_picker.delegate = self
//        cell.speed_picker.dataSource = self
        
        btn_zoom_in.layer.cornerRadius = btn_zoom_in.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)
        
        btn_zoom_out.layer.cornerRadius = btn_zoom_out.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
        
        
        self.tbl_data.register(UINib(nibName: "Date_and_Time", bundle: nil), forCellReuseIdentifier: "Date_and_Time")
        self.tbl_data.register(UINib(nibName: "Vehicle_Detail_Cell", bundle: nil), forCellReuseIdentifier: "Vehicle_Detail_Cell")
        self.tbl_data.register(UINib(nibName: "Submit_Button_Cell", bundle: nil), forCellReuseIdentifier: "Submit_Button_Cell")
        
        btn_play_pause.addTarget(self, action: #selector(Play_Pause_pressed), for: .touchUpInside)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_date_done.addTarget(self, action: #selector(pressed_done_date), for: .touchUpInside)
        vehicle_cell_height = tbl_data.frame.size.height / 1.8
        to_date = GetToDate(date : TodayToDate())
        to_time = GetToTime(time : TodayToDate())
        btn_submit_date_time.addTarget(self, action: #selector(btn_submit_pressed), for: .touchUpInside)
        txt_start_date.addTarget(self, action: #selector(tap_txt_to_date), for: .editingDidBegin)
        txt_end_date.addTarget(self, action: #selector(tap_txt_from_date), for: .editingDidBegin)
        
        from_date = GetFromDate(date: TodayFromDate())
        from_time = GetFromTime(time: TodayFromDate())
//
        layout()
        overlayView.addGestureRecognizer(panRecognizer)
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.addSubview(alert_view)
        txt_start_date.text = TodayFromDate()
       txt_end_date.text = TodayToDate()
        CallHistoryData()
    }
    
    
    @IBAction func day_select(_ sender: UISegmentedControl) {
       
        if play{
            showToast(controller: self, message : "Please stop your Vehicle.", seconds: 2.0)
            sender.selectedSegmentIndex = UISegmentedControl.noSegment
            return
        }
            switch sender.selectedSegmentIndex
            {
            case 0:
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.view_date_time_perent.frame.size.height = 0.0
//                    self.view_date_time_perent.isHidden = true
//                })
//                date_cell_height = 0.0
//                submit_cell_height = tbl_data.frame.size.height / 3.5
//                let indexPath = IndexPath(row: 0, section: 0)
//                let cell = tbl_data.cellForRow(at: indexPath) as! Date_and_Time
                txt_start_date.text = TodayFromDate()
                txt_end_date.text = TodayToDate()
//                tbl_data.reloadData()
        
                
                
                from_date = GetToDate(date : txt_start_date.text!)
                from_time = GetToTime(time : txt_start_date.text!)
                
                
                to_date = GetFromDate(date: txt_end_date.text!)
                to_time = GetFromTime(time: txt_end_date.text!)
                self.view.addSubview(alert_view)
                // tbl_data.reloadData()
                CallHistoryData()
                
                break
            case 1:
               
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.view_date_time_perent.frame.size.height = 0.0
//                    self.view_date_time_perent.isHidden = true
//                })
//                date_cell_height = 0.0
//                submit_cell_height = tbl_data.frame.size.height / 3.5
//                let indexPath = IndexPath(row: 0, section: 0)
//                let cell = tbl_data.cellForRow(at: indexPath) as! Date_and_Time
                txt_start_date.text = YesterdayFromDate()
                txt_end_date.text = YesterdayToDate()
          
                
                
                from_date = GetToDate(date : txt_start_date.text!)
                from_time = GetToTime(time : txt_start_date.text!)
                
                
                to_date = GetFromDate(date: txt_end_date.text!)
                to_time = GetFromTime(time: txt_end_date.text!)
                self.view.addSubview(alert_view)
                // tbl_data.reloadData()
                CallHistoryData()
//                tbl_data.reloadData()
                break
            case 2:
                view_show_date_submit.isHidden = false
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.view_date_time_perent.frame.size.height = self.view_all_detail.frame.size.height / 5
//                    self.view_date_time_perent.isHidden = false
//                })
          
//                date_cell_height = tbl_data.frame.size.height / 2.5
//                submit_cell_height = tbl_data.frame.size.height / 3.5
//                tbl_data.reloadData()
                break
            default:
                break;
            }
        
        
     
    }
    
//     @objc func addAnimation(){
//        let animator = UIViewPropertyAnimator(duration: 1, curve: .easeOut, animations: {
//            self.view_show_history.transform = CGAffineTransform(scaleX: 1.6, y: 1.6).concatenating(CGAffineTransform(translationX: 0, y: 15))
//            self.view_show_history.transform = .identity
//        })
//        animator.startAnimation()
//    }
    
    @IBAction func pressed_done(_ sender: Any) {
        view_date_and_time_perent.isHidden = true
    }
    
  
    
    func layout(){
        view_show_history.translatesAutoresizingMaskIntoConstraints = false
        view_show_history.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        view_show_history.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = view_show_history.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        view_show_history.heightAnchor.constraint(equalToConstant: 300).isActive = true
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
             txt_start_date.text = dateFormatter.string(from: sender.date)
        }else{
            txt_end_date.text = dateFormatter.string(from: sender.date)
        }}
    

    private var currentState: State = .closed
    
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
                self.view_show_history.layer.cornerRadius = 20
              
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                self.view_show_history.layer.cornerRadius = 0
              
            }
            self.view.layoutIfNeeded()
        })
        
        transitionAnimator.addCompletion { position in
            
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
            }
            
            self.runningAnimators.removeAll()
            
        }
        
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        
        
        inTitleAnimator.scrubsLinearly = false
        
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
           
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
          
            runningAnimators.forEach { $0.pauseAnimation() }
           
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
          
            let translation = recognizer.translation(in: view_show_history)
            var fraction = -translation.y / popupOffset
            
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
       
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
          
            let yVelocity = recognizer.velocity(in: view_show_history).y
            let shouldClose = yVelocity > 0
            
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
      
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
          
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
}


//class InstantPanGestureRecognizer: UIPanGestureRecognizer {
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
//        if (self.state == UIGestureRecognizer.State.began) { return }
//        super.touchesBegan(touches, with: event)
//        self.state = UIGestureRecognizer.State.began
//    }
//    
//}

extension TrackingStatusHistoryVC{
    
    func CallHistoryData(){
        showMap.clear()
        marker.map = nil
        ignition_status.removeAll()
        speed_limit.removeAll()
        latitude.removeAll()
        longitude.removeAll()
        direction.removeAll()
        place.removeAll()
        start_date.removeAll()
        self.rectangle_green.map = nil
        self.rectangle_red.map = nil
        self.g_timer.invalidate()
        data.removeAll()
        iTemp = 0
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        let urlString = domain_name + Track_History + self.device_id! + "&date_from=" + from_date + from_time + "&date_to=" + to_date + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        
       
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        //print("skll \(String(describing: encodedString))")

        CallTrackResult(urlString: encodedString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                print("result :\(result)")
                
                switch (result){
                    
                case 0 :
                    
                    UIView.animate(withDuration: 0.0, animations: {
                        let c_data = data?[K_Data] as! Dictionary<String,Any>
                        
                        print("total data \(c_data)")
                        
                        let trip_data = c_data["trip_data"] as! Array<Any>
                        //print("dkjklsdjklds \(trip_data)")
                        
                        self.VehicleName = GetRegistrationNumber(Vehicals: c_data)
                        self.lbl_vehicle_name.text = self.VehicleName
                        self.distance = GetSumOfDistance(Vehicals: c_data)
                        self.lbl_distance.text = self.distance + "KM"
                        if let hours = c_data["total_working_hours"] as? String{
                            self.working_hour = hours
                        }else{
                            self.working_hour = String(c_data["total_working_hours"] as! Int)
                        }
                        
                        let trip_count = c_data["trip_count"] as! Int
                        self.lbl_trip.text = String(trip_count)
                        self.lbl_time.text = self.working_hour
                        
                        for val in trip_data{
                            let val_data = val as! Dictionary<String, Any>
                            let latitude = val_data["latitude"] as! Double
                            let longitude = val_data["longitude"] as! Double
                            let speed = Double(val_data["speed"] as! String)
                            let vehicle_status = val_data["vehicle_status"] as! Int
                            let angle = Double(val_data["direction"] as! String)
                          
                            let newDetail = TripVehicle(speed: speed!, status: vehicle_status, latitude: latitude, longitude: longitude, direction: angle!, start_date: val_data["start_date"] as! String, place: val_data["place"] as! String)
                            
                            self.data.append(newDetail)
                            
                        }
                    }, completion: { _ in
                        //self.SetCoordinatestoValue()
                      //  self.mapView.delegate = self
                        
                        //print("Complete Task")
                  //  DispatchQueue.global(qos: .background).async {
                       DispatchQueue.main.async {
                            self.alert_view.removeFromSuperview()

                            if self.data.count > 0{
                                self.drawPathOnMap()
                                self.showMap.isHidden = false
                            }else{
                                self.showMap.isHidden = true
                            }
                        }
                    })
                    break
                case _ where result > 0  :
                     self.distance = "-----"
                     self.working_hour = "-----"
                     self.lbl_distance.text = "----"
                     self.lbl_time.text = "----"
                     self.lbl_trip.text = "--"
                    let message = data?[K_Message] as! String
                    print(message)
                     self.SetCoordinatestoValue()
                     self.mapView.delegate = self
                     self.alert_view.removeFromSuperview()
                     
                     
                     if self.latitude.count > 0{
                        self.drawPathOnMap()
                        self.showMap.isHidden = false
                     }else{
                        self.showMap.isHidden = true
                     }
                    break
                default:
                    self.distance = "-----"
                    self.working_hour = "-----"
                    self.lbl_distance.text = "----"
                    self.lbl_time.text = "----"
                    self.lbl_trip.text = "--"
                    self.SetCoordinatestoValue()
                    self.mapView.delegate = self
                    self.alert_view.removeFromSuperview()
                    
                    
                    if self.latitude.count > 0{
                        self.drawPathOnMap()
                        self.showMap.isHidden = false
                    }else{
                        self.showMap.isHidden = true
                    }
                    //print("Default Case66666")
                }
            }else{
                self.distance = "-----"
                self.working_hour = "-----"
                self.lbl_distance.text = "----"
                self.lbl_time.text = "----"
                self.lbl_trip.text = "--"
               // print("ERROR FOUND666666")
                self.SetCoordinatestoValue()
                self.mapView.delegate = self
                self.alert_view.removeFromSuperview()
                
                
                if self.latitude.count > 0{
                    self.drawPathOnMap()
                    self.showMap.isHidden = false
                }else{
                    self.showMap.isHidden = true
                }
            }
        
        })
    }
    
    func SetCoordinatestoValue(){
        
        var s_latitude = 0.0
        var s_longitude = 0.0
      
      //var coordinates: [(column: Double, row: Double)]?
        
        for index in 0..<self.latitude.count{
            
            s_latitude = latitude[0]
            s_longitude = longitude[0]
            
//            let lat  =  -122.63748
//            let long  = 45.52214
//            var coo = [Double.self, Double.self]
            
            
            //let coordinates = [(lat, long)].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
            //self.allCoordinates.append(coordinate)
            let coordinate = CLLocationCoordinate2D(latitude: latitude[index], longitude: longitude[index])
            self.allCoordinates.append(coordinate)
        }
       // self.allCoordinates = [(latitude), (longitude)].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
        //self.allCoordinates = self.coordinates
       // print("sdsdjsk \(String(describing: self.allCoordinates))")
        mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.setUserTrackingMode(.none, animated: true)
        mapView.setCenter(
            CLLocationCoordinate2D(latitude: s_latitude, longitude: s_longitude),
            zoomLevel: 11,
            animated: false)
        map_view.addSubview(mapView)
        self.mapView.delegate = self
    }
    
    func addPolyline(to style: MGLStyle) {
        // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
        let source = MGLShapeSource(identifier: "polyline", shape: nil, options: nil)
        style.addSource(source)
        polylineSource = source
        
        // Add a layer to style our polyline.
        let layer = MGLLineStyleLayer(identifier: "polyline", source: source)
        layer.lineJoin = NSExpression(forConstantValue: "round")
        layer.lineCap = NSExpression(forConstantValue: "round")
        layer.lineColor = NSExpression(forConstantValue: UIColor.red)
      // layer.predicate = NSPredicate(format: "%K == %@", "trail-type", "mountain-biking")
        // The line width should gradually increase based on the zoom level.
        layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'exponential', 1.5, %@)",
                                       [24: 5, 30: 20])
        style.addLayer(layer)
    }

    func animatePolyline() {
        currentIndex = 1
        
        // Start a timer that will simulate adding points to our polyline. This could also represent coordinates being added to our polyline from another source, such as a CLLocationManagerDelegate.
        
            self.timer = Timer.scheduledTimer(timeInterval: 0.00005, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
      
    }
    
    @objc func tick() {
        if currentIndex > allCoordinates.count {
            timer?.invalidate()
            timer = nil
            return
        }
      
        // Create a subarray of locations up to the current index.
//        for index in 0..<latitude.count{
//            let coordinates = [(latitude[index], longitude[index])].map({CLLocationCoordinate2D(latitude: $0.1, longitude: $0.0)})
//            self.updatePolylineWithCoordinates(coordinates: coordinates)
//
//        }
        
        let coordinates = Array(self.allCoordinates[0..<self.currentIndex])
       
        // Update our MGLShapeSource with the current locations.
        self.updatePolylineWithCoordinates(coordinates: coordinates)
        
            self.currentIndex += 1
       
       
    }
    
    func updatePolylineWithCoordinates(coordinates: [CLLocationCoordinate2D]) {
        var mutableCoordinates = coordinates
    
        let polyline = MGLPolylineFeature(coordinates: &mutableCoordinates, count: UInt(mutableCoordinates.count))
        
            self.polylineSource?.shape = polyline
       
    }
    
}


//extension TrackingStatusHistoryVC : UITableViewDelegate, UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let date_time_cell = tbl_data.dequeueReusableCell(withIdentifier: "Date_and_Time") as! Date_and_Time
//        let detail_cell = tbl_data.dequeueReusableCell(withIdentifier: "Vehicle_Detail_Cell") as! Vehicle_Detail_Cell
//        let submit_cell = tbl_data.dequeueReusableCell(withIdentifier: "Submit_Button_Cell") as! Submit_Button_Cell
//
//        if indexPath.row == 0{
//
//            to_date = GetToDate(date : date_time_cell.txt_to_date_time.text!)
//            to_time = GetToTime(time : date_time_cell.txt_to_date_time.text!)
//
//
//           from_date = GetFromDate(date: date_time_cell.txt_from_date_time.text!)
//             from_time = GetFromTime(time: date_time_cell.txt_from_date_time.text!)
//
//            date_time_cell.txt_to_date_time.addTarget(self, action: #selector(tap_txt_to_date), for: .editingDidBegin)
//            date_time_cell.txt_from_date_time.addTarget(self, action: #selector(tap_txt_from_date), for: .editingDidBegin)
//
//            if date_cell_height == 0.0{
//                date_time_cell.view_content.isHidden = true
//            }else{
//                date_time_cell.view_content.isHidden = false
//            }
//
//            return date_time_cell
//        }
//        if indexPath.row == 1{
//            submit_cell.btn_submit.addTarget(self, action: #selector(btn_submit_pressed), for: .touchUpInside)
//            return submit_cell
//        }else{
//            detail_cell.lbl_registration.text = VehicleName
//            detail_cell.lbl_distance.text = distance
//            detail_cell.lbl_time.text = working_hour
//
//
//            detail_cell.speed_picker.delegate = self
//
//
//            if iTemp > 0{
//                detail_cell.lbl_location.text = place[iTemp]
//                detail_cell.lbl_date_time.text = start_date[iTemp]
//                detail_cell.lbl_speed.text = String(self.speed_limit[iTemp]) + "KMPH"
//
//            }
//            if play{
//                if iTemp > 0{
//                   //  detail_cell.btn_play_pause.setImage(UIImage(named: PAUSE), for: .normal)
//                }
//
//            }else{
//               // detail_cell.btn_play_pause.setImage(UIImage(named: PLAY), for: .normal)
//            }
//
//
//           // detail_cell.btn_play_pause.addTarget(self, action: #selector(Play_Pause_pressed), for: .touchUpInside)
//
//            return detail_cell
//        }
//
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if indexPath.row == 0{
//             return date_cell_height
//        }
//        if indexPath.row == 1{
//         return submit_cell_height
//        }else{
//             return vehicle_cell_height
//                //tbl_data.frame.size.height / 1.3
//        }
//
//
//    }
//
//
//}

extension TrackingStatusHistoryVC : MGLMapViewDelegate{
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        let camera = MGLMapCamera(lookingAtCenter: mapView.centerCoordinate, altitude: 1500, pitch: 15, heading: 360)
        mapView.setCamera(camera, withDuration: 5, animationTimingFunction: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
            self.addPolyline(to: self.mapView.style!)
            self.animatePolyline()
    }
    
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        let source = MGLRasterTileSource(identifier: IDENTIFIER, tileURLTemplates: [Title_URL], options: [ .tileSize: 256 ])
        let rasterLayer = MGLRasterStyleLayer(identifier: IDENTIFIER, source: source)
        style.addSource(source)
        style.addLayer(rasterLayer)
        self.rasterLayer = rasterLayer
    }
    
  

}

extension TrackingStatusHistoryVC{
    
    
    @objc func pressed_zoom_in(){
        if showMap.isHidden{
            return
        }
        let zoom_in = self.showMap.camera.zoom + 2
        let loc : CLLocation = CLLocation(latitude: self.data[iTemp].latitude, longitude: self.data[iTemp].longitude)
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
        
    }
    
    @objc func pressed_zoom_out(){
        if showMap.isHidden{
            return
        }
        let loc : CLLocation = CLLocation(latitude: self.data[iTemp].latitude, longitude: self.data[iTemp].longitude)
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    @objc func tap_txt_to_date(sender: UITextField){
        sender.resignFirstResponder()
        isFromDate = true
       
        UIView.animate(withDuration: 0.5, animations: {
            self.view_select_date_time.frame.origin.y = 10
        }, completion: { _ in
           self.sub_view_date.isHidden = false
            
        })
       
        //view_date_and_time_perent.isHidden = false
    }
    
    @objc func tap_txt_from_date(sender: UITextField){
        sender.resignFirstResponder()
        isFromDate = false
        UIView.animate(withDuration: 0.5, animations: {
            self.view_select_date_time.frame.origin.y = 10
        }, completion: { _ in
            self.sub_view_date.isHidden = false
            
        })
       // view_date_and_time_perent.isHidden = false
    }
    
    @objc func Play_Pause_pressed(){
        
        PlayOrPause()
       
    }
    
    func PlayOrPause(){
        if play{
            g_timer.invalidate()
            play = false
            if iTemp > 0{
            btn_play_pause.setImage(UIImage(named: PLAY), for: .normal)
            }
        }else{
            btn_play_pause.setImage(UIImage(named: PAUSE), for: .normal)
            g_timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (_) in
                self.playCar()
            })

            RunLoop.current.add(g_timer, forMode: .common)
            play = true
            
        }
        

    }
    
    @objc func pressed_done_date(){
        self.sub_view_date.isHidden = true
        UIView.animate(withDuration: 0.5, animations: {
              self.view_select_date_time.frame.origin.y = self.view_show_date_submit.frame.size.height / 4
        })
    }
    
    @objc func btn_submit_pressed(){
        
        if txt_end_date.text! <  txt_start_date.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        if txt_end_date.text! ==  txt_start_date.text!{
            showToast(controller: self, message: "End Date should be greater then Start Date.", seconds: 1.5)
            return
        }
        
        self.sub_view_date.isHidden = true
        self.view_select_date_time.frame.origin.y = view_show_date_submit.frame.size.height / 4
        from_date = GetToDate(date : txt_start_date.text!)
        from_time = GetToTime(time : txt_start_date.text!)
        
        to_date = GetFromDate(date: txt_end_date.text!)
        to_time = GetFromTime(time: txt_end_date.text!)
        self.view.addSubview(alert_view)
    
        view_show_date_submit.isHidden = true
        CallHistoryData()
        
    }
    
    // ---------------------------------- Google Map --------------------------------- //
    
    func drawPathOnMap()  {
       
        UIView.animate(withDuration: 0.0, animations: {
            //showToast(controller: self, message: "Please Wait...", seconds: 0.0)
            // print("Complete Task 123")
        }, completion: { _ in
          //DispatchQueue.global(qos: .background).async {
            //DispatchQueue.main.async {
                let path_green = GMSMutablePath()
                let path_red = GMSMutablePath()
                var marker = GMSMarker()
                
                UIView.animate(withDuration: 0.0, animations: {
                    self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.data[0].latitude, longitude: self.data[0].longitude))
                    let camera = GMSCameraPosition.camera(withLatitude: self.data[0].latitude, longitude: self.data[0].longitude, zoom: 14)
                    self.showMap!.camera = camera
                }, completion: { _ in
                    
                    UIView.animate(withDuration: 0.0, animations: {
                        
                        for val in 0..<2{
                            
                            if val == 0{
                                let initialLocation = CLLocationCoordinate2DMake(self.data[0].latitude, self.data[0].longitude)
                                marker = GMSMarker(position: initialLocation)
                                marker.icon = UIImage(named: "startPoint")
                                marker.map = self.showMap
                            }
                            if val == 1{
                                
                                let initialLocation = CLLocationCoordinate2DMake(self.data[self.data.count - 1].latitude, self.data[self.data.count - 1].longitude)
                                marker = GMSMarker(position: initialLocation)
                                marker.icon = UIImage(named: "endPoint")
                                marker.map = self.showMap
                            }
                            
                        }
                        for (index, _) in self.data.enumerated()
                        {
                            if self.data[index].status == 1{
                                let initialLocation = CLLocationCoordinate2DMake(self.data[index].latitude, self.data[index].longitude)
                                marker = GMSMarker(position: initialLocation)
                                marker.icon = UIImage(named: "ignition_mark")
                                marker.map = self.showMap
                            }
                        }
                        
                        
                    }, completion: { _ in
                        let inialLat:Double = self.data[0].latitude
                        let inialLong:Double = self.data[0].longitude
                        
                        UIView.animate(withDuration: 0.0, animations: {
                            for (index, _) in self.data.enumerated()
                            {
                                let solidRed = GMSStrokeStyle.solidColor(.red)
                                let solidBlue = GMSStrokeStyle.solidColor(Moving_Color)
                             
                                if self.data[index].speed > 60.0{
                                    if index > 2{
                                        path_red.add(CLLocationCoordinate2DMake(self.data[index - 1].latitude, self.data[index - 1].longitude))
                                    }
                                    path_red.add(CLLocationCoordinate2DMake(self.data[index].latitude, self.data[index].longitude))
                                    if index < self.data.count - 2{
                                        path_red.add(CLLocationCoordinate2DMake(self.data[index + 1].latitude, self.data[index + 1].longitude))
                                    }
                                    self.rectangle_green = GMSPolyline(path: path_red)
                                    self.rectangle_green.strokeWidth = 5.0
                                    self.rectangle_red.geodesic = true
                                    self.rectangle_green.spans = [GMSStyleSpan(style: solidRed)]
                                 
                                    path_green.removeAllCoordinates()
                                }else{
                                    if index > 2{
                                        path_green.add(CLLocationCoordinate2DMake(self.data[index - 1].latitude, self.data[index - 1].longitude))
                                    }
                                    
                                    path_green.add(CLLocationCoordinate2DMake(self.data[index].latitude, self.data[index].longitude))
                                    self.rectangle_green = GMSPolyline(path: path_green)
                                    self.rectangle_green.strokeWidth = 5.0
                                    self.rectangle_green.geodesic = true
                                    self.rectangle_green.spans = [GMSStyleSpan(style: solidBlue)]
                                    path_red.removeAllCoordinates()
                                }
                                self.rectangle_green.map = self.showMap
                            }
                        }, completion: { _ in
                          DispatchQueue.main.async {
                            
                            UIView.animate(withDuration: 0.0, animations: {
                                marker.map = self.showMap
                                
                                
                            }, completion: { _ in
                                let loc : CLLocation = CLLocation(latitude: inialLat, longitude: inialLong)
                                 self.updateMapFrame(newLocation: loc, zoom: 18.0)
                                
                            })
                            
                           }
                          
                        })
                       
                    })

                })
        })

    }
    
    
    func playCar()
    {
        if iTemp <= (self.data.count - 1 )
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.marker.groundAnchor = CGPoint(x: 0.5, y: 0.3)
                self.marker.rotation = self.data[self.iTemp].direction
                self.showMap.animate(toBearing: 0)
            }, completion: { _ in
                self.MoveTheCar()
            })
        }
    }
    
    
    func MoveTheCar(){
        
        self.lbl_update_distance.text = String(self.data[iTemp].speed) + "KMPH"
            ///String(self.speed_limit[iTemp]) + "KMPH"
        self.lbl_update_time.text = self.data[iTemp].start_date
            //self.start_date[iTemp]
        self.lbl_update_location.text = self.data[iTemp].place
            //self.place[iTemp]
     
        let loc : CLLocation = CLLocation(latitude: self.data[iTemp].latitude, longitude: self.data[iTemp].longitude)
        updateMapFrame(newLocation: loc, zoom: self.showMap.camera.zoom)
        marker.position = CLLocationCoordinate2DMake(self.data[iTemp].latitude, self.data[iTemp].longitude)
        
//        var vehicle_name = "car"
//        if vehicle_type_id == "56"{
//            vehicle_name = "police"
//        }else if vehicle_type_id == "1"{
//            vehicle_name = "car"
//        }else if vehicle_type_id == "2"{
//            vehicle_name = "truck"
//        }else if vehicle_type_id == "3"{
//            vehicle_name = "bus"
//        }else if vehicle_type_id == "4"{
//            vehicle_name = "van"
//        }else if vehicle_type_id == "5"{
//            vehicle_name = "car"
//        }else if vehicle_type_id == "6"{
//            vehicle_name = "bike"
//        }else if vehicle_type_id == "7"{
//            vehicle_name = "scooty"
//        }else{
//            vehicle_name = "car"
//        }
        
        marker.icon = UIImage(named: vehicle_type_id)
        marker.setIconSize(scaledToSize: .init(width: 20, height: 40))
        marker.map = showMap
        
  
        if iTemp == (self.latitude.count - 1)
        {
            // timer close
            g_timer.invalidate()
            //buttonPlay.isEnabled = true
            iTemp = 0
        }
        iTemp += 1
    }
 
    
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.showMap.animate(to: camera)
    }
}

extension TrackingStatusHistoryVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return speed_x.count
    }
    
   
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        interval = time_interval[row]
        g_timer.invalidate()
        btn_play_pause.setImage(UIImage(named: "play"), for: .normal)
        play = false
        self.Play_Pause_pressed()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 15))
        let label_speed: UILabel = UILabel(frame: CGRect(x:0, y: 0, width: pickerLabel.frame.size.width, height: pickerLabel.frame.size.height))
        label_speed.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
        label_speed.text = speed_x[row]
        label_speed.textAlignment = .center
        pickerLabel.addSubview(label_speed)
        
        return pickerLabel
    }
    
}


