//
//  NotificationSettingVC.swift
//  GeoTrack
//
//  Created by Georadius on 14/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class NotificationSettingVC: UIViewController {

    @IBOutlet weak var tbl_notification: UITableView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    
    let alert_view = AlertView.instanceFromNib()
    var date = [String]()
    var notification_name = [String]()
    var days = [String]()
    var app_notification = [String]()
    var alert_type_id = [String]()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       SideMenu()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.tbl_notification.register(UINib(nibName: "NotificationSettingCell", bundle: nil), forCellReuseIdentifier: "NotificationSettingCell")
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tbl_notification.addSubview(refreshControl)
       
        //btn_menu_pressed.addTarget(self, action: #selector(back_button_pressed), for: .touchUpInside)
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        CallDataFromServer()
    }

    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        CallDataFromServer()
    }
    
    @objc func back_button_pressed(){
     
        self.navigationController?.popViewController(animated: true)
    }
    
    func SideMenu(){
           btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
           revealViewController()?.rearViewRevealWidth = 250
       }
    
    func CallDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_notification.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        
        date.removeAll()
        notification_name.removeAll()
        days.removeAll()
        app_notification.removeAll()
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + Notification_Setting + "user_name=" + user_name + "&hash_key=" + hash_key + "&user_id=" + user_id
        
        CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.notification_name .append(val_data["alert_type_name"] as! String)
                    self.alert_type_id.append(val_data["alert_type_id"] as! String)
                    let start_date = GetNotificatioDate(notification: val_data, key: "active_time_start")
                    let end_date = GetNotificatioDate(notification: val_data, key: "active_time_end")
                    let date_time = start_date + " - " + end_date
                    self.date.append(date_time)
                    self.days.append(val_data["active_days_binary"] as! String)
                    self.app_notification.append(GetAppNotification(notification: val_data))
                }
                
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.tbl_notification.delegate = self
            self.tbl_notification.dataSource = self
            self.tbl_notification.reloadData()
            self.refreshControl.endRefreshing()
            self.alert_view.removeFromSuperview()
        })
      
    }
    
    @objc func Switch_Triggered(sender: UISwitch){
        
        let days_status = [Character]()
        let binary = days[sender.tag].dropFirst()
        let revert_binary = binary.reversed()
        
        if sender.isOn{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingDetail") as! NotificationSettingDetail
            vc.alert_id = self.alert_type_id[sender.tag]
            
            vc.days_status.removeAll()
            for (_, val) in revert_binary.enumerated(){
                vc.days_status.append(String(val))
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
           let full_date = date[sender.tag].components(separatedBy: " ")
           let alert_id = self.alert_type_id[sender.tag]
            UpdateDataOnServer(days_status: days_status, start_date: full_date[0], end_date: full_date[2], alert_id: alert_id)
            
        }
    }
    
    func UpdateDataOnServer(days_status: [Character], start_date: String, end_date: String, alert_id: String){
        self.view.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
     
        
        let url1 = domain_name + "/user_alert_permissions_result.php?data_format=1&action=alertassignment" + "&user_name=" + user_name + "&hash_key=" + hash_key + "&alert_type_id=" + alert_id + "&sms_notification=0&email_notification=1&app_notification=0"
        
        let url2 = "&monday=" + "0" + "&tuesday=" + "0" + "&wednesday=" + "0" + "&thursday=" + "0" + "&friday=" + "0" + "&saturday=" + "0"
        
        let url3 = "&sunday=" + "0" + "&active_time_start=" + "00:00:00" + "&active_time_end=" + "00:00:00" + "&email=&phone=&user_id=" + user_id
        
        let urlString = url1 + url2 + url3
        
    
        CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.alert_view.removeFromSuperview()
               // showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.CallDataFromServer()
        })
        
    }
}




extension NotificationSettingVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notification_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_notification.dequeueReusableCell(withIdentifier: "NotificationSettingCell") as! NotificationSettingCell
        cell.lbl_notification_name.text = notification_name[indexPath.row]
        cell.lbl_date.text = date[indexPath.row]
        cell.switch_setting.addTarget(self, action: #selector(Switch_Triggered(sender:)), for: .valueChanged)
        cell.switch_setting.tag = indexPath.row
        let binary = days[indexPath.row].dropFirst()
        let revert_binary = binary.reversed()
        for (index, val) in revert_binary.enumerated(){
            if val == "1"{
                cell.lbl_days[index].backgroundColor = Global_Blue_Color
            }else{
                cell.lbl_days[index].backgroundColor = UIColor.darkGray
            }
        }
        if app_notification[indexPath.row] == "1"{
            cell.switch_setting.isOn = true
        }else{
            cell.switch_setting.isOn = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_notification.frame.size.height / 5
    }
    
}

