//
//  ViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl_Login_GeoRadius: UILabel!
    @IBOutlet weak var tbl_language: UITableView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var view_card_login: UIView!
    @IBOutlet weak var lbl_Not_login: UILabel!
    @IBOutlet weak var lbl_user_error: UILabel!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var lbl_password_error: UILabel!
    @IBOutlet weak var lbl_country_id: UILabel!
    @IBOutlet weak var login_card_view: UIView!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var txt_contry_id: UITextField!
    @IBOutlet weak var txt_username: UITextField!
    let alert_view = AlertView.instanceFromNib()
   
    override func viewDidLoad() {
        super.viewDidLoad()
      UserDefaults.standard.set(0, forKey: "select_home_page")
        UserDefaults.standard.set(false, forKey: "is_global_selection")
       //Set_Gradiant_Color()
       Set_Layer()
       btn_login.addTarget(self, action: #selector(pressed_login), for: .touchUpInside)
       SetTextFieldLeftSide(imageName : "username", txt_field: txt_username)
       SetTextFieldLeftSide(imageName : "password", txt_field: txt_password)
       SetTextFieldLeftSide(imageName : "companyid", txt_field: txt_contry_id)
       SetCompnyID()
       SetLanguage()
       img_icon.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
       view_card_login.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
   }
    
    func SetLanguage(){
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        txt_username.placeholder = bd.localizedString(forKey: "USERNAME", value: nil, table: nil)
        txt_password.placeholder = bd.localizedString(forKey: "PASSWORD", value: nil, table: nil)
        txt_contry_id.placeholder = bd.localizedString(forKey: "COMPANY_ID", value: nil, table: nil)
        let name = bd.localizedString(forKey: "LOGIN", value: nil, table: nil)
        btn_login.setTitle(name, for: .normal)
        lbl_Login_GeoRadius.text = bd.localizedString(forKey: "LOGIN_TO_GEOTRACK", value: nil, table: nil)
        
    }
    
    func Set_Layer(){
        Set_Card_View(Card: login_card_view)
        Set_Login_Radius(Button: btn_login)
    }

    func Set_Gradiant_Color(){
        let colors = Colors()
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer!.frame = view.frame
        view.layer.insertSublayer(backgroundLayer!, at: 0)
    }
    
    func SetCompnyID(){
        if UserDefaults.standard.value(forKey: CountryID) != nil{
            let id = UserDefaults.standard.value(forKey: CountryID) as! String
                txt_contry_id.text = id
        }
        
    }
    
    @IBAction func pressed_forgot(_ sender: Any) {
        
         if (txt_contry_id.text?.count)! < 1{
            lbl_country_id.isHidden = false
            return
        }
        
        lbl_country_id.isHidden = true
        CallWebserviceGetDomain(isForgotPassword: true)
     
    }
    @objc func pressed_login(){
       
        if (txt_username.text?.count)! < 1{
            lbl_user_error.isHidden = false
            return
        }else{
            lbl_user_error.isHidden = true
        }
        
        if (txt_password.text?.count)! < 1{
            lbl_password_error.isHidden = false
            return
        }else{
            lbl_password_error.isHidden = true
        }
        if (txt_contry_id.text?.count)! < 1{
            lbl_country_id.isHidden = false
            return
        }else{
            lbl_country_id.isHidden = true
            SaveData(country_id: txt_contry_id.text!)
        }
        CallWebserviceGetDomain(isForgotPassword: false)
        
    }
    @IBAction func pressed_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CallWebserviceGetDomain(isForgotPassword: Bool){
        let v_id = txt_contry_id.text!
        let urlString = Base_Url + v_id
        self.view.addSubview(alert_view)
    
        CallWebService(urlString: urlString, completionHandler: {data, r_error, isNetwork in
           
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String, Any>
                    let domain = c_data[K_Domain] as! String
                    print(domain)
                    let domain_name = "https://"+domain
                    UserDefaults.standard.set(domain_name, forKey: DOMAIN_NAME)
                    UserDefaults.standard.set(domain, forKey: "Domain_s")
                    
                    self.lbl_Not_login.isHidden = true
                    
                    if isForgotPassword{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                       self.CallWebServiceAuthanticate(domain: domain_name)
                    }
                    
                    break
                case _ where result > 0 :
                   self.lbl_Not_login.isHidden = false
                    
                    break
                default:
                    self.lbl_Not_login.isHidden = false
                    print("Default Case")
                }
                
               
            }else{
                self.alert_view.removeFromSuperview()
                self.lbl_Not_login.isHidden = false
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    func CallWebServiceAuthanticate(domain: String){
        let username = txt_username.text!
        let password = txt_password.text!
        
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as? String ?? ""
        
        print("device token \(device_token) ")
      
        let urlString = domain + Authenticate_Url_Username + username + Authenticate_Url_Password + password + "&user_app_id=" + device_token
       
        CallWebService(urlString: urlString, completionHandler: {data, r_error, isNetwork in

            if isNetwork{
            
            print(String(describing: data))
            let result = data?[K_Result] as! Int
            print(result)
            switch (result){
            case 0 :
                let c_data = data?[K_Data] as! Dictionary<String,Any>
                print(c_data)
                self.lbl_Not_login.isHidden = true
                let login_key = c_data["hash_key"] as! String
                let user_name = c_data["user_name"] as! String
                let phone = c_data["phone"] as! String
                let user_id = c_data["user_id"] as! String
                let group_id = c_data["group_id"] as! String
                UserDefaults.standard.set(user_name, forKey: USER_NAME)
                UserDefaults.standard.set(phone, forKey: PHONE)
                UserDefaults.standard.set(user_id, forKey: USER_ID)
                UserDefaults.standard.set(group_id, forKey: GROUP_ID)
                SaveLoginKey(key : login_key)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
                self.navigationController?.pushViewController(vc, animated: true)

            case _ where result > 0  :
                let message = data?[K_Message] as! String
                self.lbl_Not_login.isHidden = false
                print(message)
                break

            default:
                self.lbl_Not_login.isHidden = false
                print("Default Case")
            }
            }else{
                self.lbl_Not_login.isHidden = false
            }
            
             self.alert_view.removeFromSuperview()
        })
       
    }
    
}



