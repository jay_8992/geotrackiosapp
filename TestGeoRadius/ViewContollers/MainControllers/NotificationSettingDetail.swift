//
//  NotificationSettingDetail.swift
//  GeoTrack
//
//  Created by Georadius on 14/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class NotificationSettingDetail: UIViewController {

    @IBOutlet weak var view_pcker_time: UIView!
    @IBOutlet weak var txt_end_time: UITextField!
    @IBOutlet weak var txt_start_time: UITextField!
    @IBOutlet var btn_days: [UIButton]!
    @IBOutlet weak var btn_update: UIButton!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var picker_time: UIDatePicker!
    @IBOutlet weak var btn_time_done: UIButton!
    
    let alert_view = AlertView.instanceFromNib()
    var alert_id : String!
    var days_status = ["0", "0", "0", "0", "0", "0", "0"]
    var isStartDate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_update.addTarget(self, action: #selector(UpdateDataOnServer), for: .touchUpInside)
        txt_start_time.addTarget(self, action: #selector(tap_start_time), for: .editingDidBegin)
        txt_end_time.addTarget(self, action: #selector(tap_end_time), for: .editingDidBegin)
        picker_time.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_time_done.addTarget(self, action: #selector(done_pressed), for: .touchUpInside)
       
        view_pcker_time.isHidden = true
        SetbuttonTarget()
        
    }
    
    @IBAction func back_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func SetbuttonTarget(){
        for index in 0...6{
            btn_days[index].tag = index
            btn_days[index].addTarget(self, action: #selector(btn_day_pressed(sender:)), for: .touchUpInside)
        }
        SetDaysStatus()
    }
    
    func SetDaysStatus(){
        for index in 0...6{
            if days_status[index] == "0"{
                btn_days[index].setImage(UIImage(named: "uncheck"), for: .normal)
            }else{
                 btn_days[index].setImage(UIImage(named: "check"), for: .normal)
            }
        }
    }
    
    @objc func tap_start_time(){
        txt_start_time.resignFirstResponder()
        view_pcker_time.isHidden = false
        isStartDate = true
    }
    
    @objc func tap_end_time(){
        txt_end_time.resignFirstResponder()
        view_pcker_time.isHidden = false
        isStartDate = false
    }
    
    @objc func done_pressed(){
        view_pcker_time.isHidden = true
    }
   
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        if isStartDate{
            txt_start_time.text = dateFormatter.string(from: sender.date)
        }else{
            txt_end_time.text = dateFormatter.string(from: sender.date)
        }
    }
    
    
    @objc func btn_day_pressed(sender: UIButton){
        let imageNamed = btn_days[sender.tag].currentImage
        if imageNamed == UIImage(named: "check"){
            btn_days[sender.tag].setImage(UIImage(named: "uncheck"), for: .normal)
            //days_status.insert("0", at: sender.tag)
            days_status[sender.tag] = "0"
            
        }else{
           btn_days[sender.tag].setImage(UIImage(named: "check"), for: .normal)
             //days_status.insert("1", at: sender.tag)
             days_status[sender.tag] = "1"
        }
          //print("dlfdlk \(days_status)")
    }
    
    
    @objc func UpdateDataOnServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            //self.tbl_show_live_track.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let start_time = txt_start_time.text
        let end_time = txt_end_time.text
        
        let url1 = domain_name + "/user_alert_permissions_result.php?data_format=1&action=alertassignment" + "&user_name=" + user_name + "&hash_key=" + hash_key + "&alert_type_id=" + alert_id + "&sms_notification=0&email_notification=1&app_notification=1"
        
        let url2 = "&monday=" + days_status[0] + "&tuesday=" + days_status[1] + "&wednesday=" + days_status[2] + "&thursday=" + days_status[3] + "&friday=" + days_status[4] + "&saturday=" + days_status[5]
        
        let url3 = "&sunday=" + days_status[6] + "&active_time_start=" + start_time!
        
        let urlString = url1 + url2 + url3 + "&active_time_end=" + end_time! + "&email=&phone=&user_id=" + user_id
      
        
      
        CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 0.5)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 0.5)
            }
           
            self.alert_view.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        })
        
    }

}
