//
//  ShowNotificationTracking.swift
//  TestGeoRadius
//
//  Created by Georadius on 05/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ShowNotificationTracking: UIViewController {

    let alert_view = AlertView.instanceFromNib()
    
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    var alert_data_registration = [String]()
    var alert_child = [NSArray]()
    var device_id : String?
 var alert_type_id = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tbl_report.register(UINib(nibName: "AllRepeortsCell", bundle: nil), forCellReuseIdentifier: "AllRepeortsCell")
        CallAlertTypeData()
        // Do any additional setup after loading the view.
    }
    
    func CallAlertTypeData(){
        self.view.addSubview(alert_view)
        
        alert_type_id.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let vehicle_type_url = domain_name + Alert_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        
        CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        
                        self.alert_type_id.append(v_val["alert_type_id"] as! String)
                    }
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
            self.CallDataFromServer()
        })
    }
    
    
    func CallDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        alert_data_registration.removeAll()
        alert_child.removeAll()
        self.view.addSubview(alert_view)
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let from_date = GetFromDate(date: TodayFromDate())
        let to_date = GetToDate(date: TodayToDate())
        let from_time = GetFromTime(time: TodayFromDate())
        let to_time = GetToTime(time: TodayToDate())
        
        
        let alert_type_ids = alert_type_id.joined(separator: ",")
        
        let urlString1 = domain_name + Report_All + self.device_id! + "&date_from=" + from_date
        
        
        
        let urlString2 = "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&alert_type_id=" + alert_type_ids + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        let urlString = urlString1 + urlString2
        
        CallReportDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.alert_data_registration.append(GetRegistrationNumber(Vehicals: val_data))
                    self.alert_child.append(val_data["alertchild_data"] as! NSArray)
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                self.lbl_error.text = "Please Check Network Connection"
                self.tbl_report.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }



}
extension ShowNotificationTracking: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return alert_data_registration.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let total_count = alert_child[section]
        return total_count.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "AllRepeortsCell") as! AllRepeortsCell
        let data_alert = alert_child[indexPath.section][indexPath.row] as! Dictionary<String, Any>
        cell.lbl_time.text = GetAlertTime(alert_child: data_alert)
        cell.lbl_alert.text = GetAlertTypeName(alert_child: data_alert)
        cell.lbl_location.text = GetAlertLocation(alert_child: data_alert)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderViewReportAll.instanceFromNib()
        view.lbl_registration.text = alert_data_registration[section]
        return view
    }
}
