//
//  DeleteUserVC.swift
//  GeoTrack
//
//  Created by Georadius on 21/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class DeleteUserVC: UIViewController {

    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var view_header: UIView!
    let alert_view = AlertView.instanceFromNib()

    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var sub_view_delete: UIView!
    var user_id : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
             sub_view_delete.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        // Do any additional setup after loading the view.
        btn_delete.addTarget(self, action: #selector(delete_user), for: .touchUpInside)
        btn_back.addTarget(self, action: #selector(pressed_back_button), for: .touchUpInside)
    }
    

    @objc func pressed_back_button(){
        self.navigationController?.popViewController(animated: true)
    }
    

    @objc func delete_user(){
        
          if !NetworkAvailability.isConnectedToNetwork() {
                  showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
                  self.alert_view.removeFromSuperview()
                  return
              }
      
              let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
              let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
              let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String

              let urlString = domain_name + Delete_User + "user_name=" + user_name + "&hash_key=" + hash_key + "&user_id=" + user_id + "&data_format=1"
        
              CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                  if isNetwork && data != nil{
                     
                      showToast(controller: self, message : "Deleted Successfully", seconds: 2.0)
        
                  }else{
                      
                      showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                      print("ERROR FOUND")
                  }
                  if r_error != nil{
                      showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                  }
                  
              })
       }
}
