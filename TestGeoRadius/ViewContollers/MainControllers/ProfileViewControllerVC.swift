//
//  ProfileViewControllerVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ProfileViewControllerVC: UIViewController {

  
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var lbl_default_page: UITextField!
    @IBOutlet weak var lbl_company_address: UITextField!
    @IBOutlet weak var lbl_company_name: UITextField!
    @IBOutlet weak var lbl_number: UITextField!
    @IBOutlet weak var lbl_name: UITextField!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    let alert_view = AlertView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        SettextField()
        SideMenu()
        CallDataFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    func SideMenu(){
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
    func SettextField(){
        SetTextFieldLeftSide(imageName : "username", txt_field: lbl_name)
        SetTextFieldLeftSide(imageName : "phone", txt_field: lbl_number)
        SetTextFieldLeftSide(imageName : "company", txt_field: lbl_company_name)
        SetTextFieldLeftSide(imageName : "address", txt_field: lbl_company_address)
        SetTextFieldLeftSide(imageName : "username", txt_field: lbl_default_page)
    }
    
    func CallDataFromServer(){
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
      let urlString = domain_name + "/user_result.php?action=search_user_json&user_name=" + user_name + "&hash_key=" + hash_key + "&userid=" + user_id
        CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.lbl_name.text = val_data["username"] as? String
                    self.lbl_number.text = val_data["phone"] as? String
                    self.lbl_company_name.text = val_data["company_name"] as? String
                    self.lbl_company_address.text = val_data["company_address"] as? String
                }
                
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
           
            self.alert_view.removeFromSuperview()
        })
    }
}
