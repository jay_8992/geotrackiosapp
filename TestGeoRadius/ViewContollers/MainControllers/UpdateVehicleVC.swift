//
//  UpdateVehicleVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 10/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UpdateVehicleVC: UIViewController {
    @IBOutlet weak var txt_registration: UITextField!
    @IBOutlet weak var txt_device_tag: UITextField!
    @IBOutlet weak var btn_update: UIButton!
    @IBOutlet weak var txt_voice: UITextField!
    @IBOutlet weak var txt_vehicle_type: UITextField!
    @IBOutlet weak var txt_device_serial: UITextField!
    @IBOutlet weak var tbl_show_items: UITableView!
    let alert_view = AlertView.instanceFromNib()

    @IBOutlet weak var lbl_vehicle_type: UILabel!
    @IBOutlet weak var view_vehicle_type: UIView!
    var data = [String]()
    var filterdData : [String]!
    var device_id : String!
    var vehicle_type_id = [String]()
    var vehicle_typeid : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txt_voice.addTarget(self, action: #selector(Tap_voice_no), for: .editingDidBegin)
        txt_device_serial.addTarget(self, action: #selector(Tap_device_serial), for: .editingDidBegin)
        txt_vehicle_type.addTarget(self, action: #selector(Tap_vehicle_type), for: .editingDidBegin)
        btn_update.addTarget(self, action: #selector(pressed_btn_update), for: .touchUpInside)
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_vehicle_type)
        self.tbl_show_items.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        
        
        view_vehicle_type.layer.cornerRadius = 5
        view_vehicle_type.layer.masksToBounds = true
        view_vehicle_type.layer.borderColor = UIColor.lightGray.cgColor
        view_vehicle_type.layer.borderWidth = 0.3
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(Tap_vehicle_type))
        lbl_vehicle_type.isUserInteractionEnabled = true
        lbl_vehicle_type.addGestureRecognizer(tapgesture)
    }
    
   
    
    @objc func Tap_vehicle_type(){
        txt_vehicle_type.resignFirstResponder()
        tbl_show_items.isHidden = false
        //isSelect_Vehicle = false
        CallVehicleType()
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = 0
        })
    }
    
    
    
    @objc func Tap_voice_no(){
        txt_voice.resignFirstResponder()
    }
    
    @objc func Tap_device_serial(){
       txt_device_serial.resignFirstResponder()
    }
    
    @objc func pressed_btn_update(){
        UpdateDataToServer()
    }
    

    func SetDataOnFields(registration_no: String, voice_no: String, device_tag: String, device_serial: String, device_id: String, vehicle_type_id: String){
        txt_registration.text = registration_no
        txt_voice.text = voice_no
        txt_device_tag.text = device_tag
        txt_device_serial.text = device_serial
        self.device_id = device_id
        self.vehicle_typeid = vehicle_type_id
        CallVehicleType()
    }

    func UpdateDataToServer(){
        
        if lbl_vehicle_type.text!.count < 1{
            showToast(controller: self, message: "Vehicle type can't be nil", seconds: 0.9)
            return
        }
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString = Update_Vehicle + txt_device_serial.text! + "&device_id=" + self.device_id! + "&vehicle_type=" + self.vehicle_typeid + "&registration_no=" + txt_registration.text! + "&tag=" + txt_device_tag.text! + "&user_name=" + user_name + "&hash_key=" + hash_key
        
         let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                  showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
           
        })
    }
    
    
    func CallVehicleType(){
        data.removeAll()
        self.view.addSubview(self.alert_view)
        self.alert_view.backgroundColor = UIColor.clear
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let vehicle_type_url = domain_name + Vehicle_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.data.append(v_val["vehicle_type_name"] as! String)
                         self.vehicle_type_id.append(GetVehicleTypeId(Vehicals: v_val))
                        
                          let type_id = String(GetVehicleTypeId(Vehicals: v_val))
                        
                        if type_id == self.vehicle_typeid{
                            self.lbl_vehicle_type.textColor = UIColor.black
                            self.lbl_vehicle_type.text = (v_val["vehicle_type_name"] as! String)
                        }
                        
                    }
                    
                    self.filterdData = self.data
                    self.tbl_show_items.delegate = self
                    self.tbl_show_items.dataSource = self
                    self.tbl_show_items.reloadData()
                    //self.search_items.delegate = self
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
}

extension UpdateVehicleVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_show_items.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        cell.lbl_item_name.text = filterdData[indexPath.row]
        cell.img_uncheck.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_show_items.frame.size.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = self.tbl_show_items.frame.size.height
                self.tbl_show_items.isHidden = true
            self.lbl_vehicle_type.textColor = UIColor.black
                self.lbl_vehicle_type.text = self.data[indexPath.row]
            self.vehicle_typeid = self.vehicle_type_id[indexPath.row]
        })
    }

    
}


