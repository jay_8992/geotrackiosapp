//
//  SelectLanguageVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 04/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class SelectLanguageVC: UIViewController {

    @IBOutlet weak var tbl_selectLanguage: UITableView!
    @IBOutlet weak var txt_select_language: UITextField!
    
    let language = ["English"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_select_language.addTarget(self, action: #selector(tap_text_select), for: .editingDidBegin)
        tbl_selectLanguage.delegate = self
        tbl_selectLanguage.dataSource = self
        SetTextFieldLeftSide(imageName : "language", txt_field: txt_select_language)
        
        //SetTextFieldRightSide(imageName : "downarrow", txt_field: txt_select_language)

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tbl_selectLanguage.isHidden = true
    }
    
    @IBAction func pressed_selectlanguage(_ sender: Any) {
        tbl_selectLanguage.isHidden = false
    }
    
    @objc func tap_text_select(){
        txt_select_language.resignFirstResponder()
    }

}

extension SelectLanguageVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return language.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_selectLanguage.dequeueReusableCell(withIdentifier: "cell_language")
        cell?.textLabel?.text = language[indexPath.row]
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 12)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tbl_selectLanguage.isHidden = true
        txt_select_language.text = language[indexPath.row]
        
        if indexPath.row == 0{
            let path = Bundle.main.path(forResource: "en", ofType: "lproj")
            _ = Bundle.init(path: path!)! as Bundle
         

            UserDefaults.standard.set("en", forKey: "language")
        }else if indexPath.row == 1{
            let path = Bundle.main.path(forResource: "th-TH", ofType: "lproj")
            _ = Bundle.init(path: path!)! as Bundle
        
       UserDefaults.standard.set("th-TH", forKey: "language")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
