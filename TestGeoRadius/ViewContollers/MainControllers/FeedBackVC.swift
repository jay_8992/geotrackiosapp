//
//  FeedBackVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FeedBackVC: UIViewController {

    @IBOutlet weak var btn_camera: UIButton!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tbl_issue_type: UITableView!
    @IBOutlet weak var lbl_enter_months: UILabel!
    
    @IBOutlet weak var txtview_description: UITextView!
    @IBOutlet weak var view_enter_months: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_issue_type: UITextField!
    @IBOutlet weak var img_feedback: UIImageView!
    
    var picker = UIImagePickerController()
    
    let issue_type = ["App Crash related", "Vehicle Location related", "Vehicle Tracking Related", "Vehicle Reports related"]
    
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    
    let alert_view = AlertView.instanceFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenu()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        tbl_issue_type.isHidden = true
        tbl_issue_type.delegate = self
        tbl_issue_type.dataSource = self
        btn_camera.layer.cornerRadius = btn_camera.frame.size.width / 2
        btn_camera.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_camera.addTarget(self, action: #selector(pressed_camera_button), for: .touchUpInside)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        txt_issue_type.addTarget(self, action: #selector(tap_issue_type), for: .editingDidBegin)
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_issue_type)
        
        
        view_enter_months.layer.cornerRadius = 5
        view_enter_months.layer.masksToBounds = true
        view_enter_months.layer.borderColor = UIColor.lightGray.cgColor
        view_enter_months.layer.borderWidth = 0.3
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tap_issue_type))
        lbl_enter_months.isUserInteractionEnabled = true
        lbl_enter_months.addGestureRecognizer(tapgesture)
        
        txtview_description.delegate = self
        
        txtview_description.text = "Type Your Comment"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func SideMenu(){
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
}

extension FeedBackVC{
    
    @objc func submit_pressed(){
        self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            alert_view.removeFromSuperview()
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            return
        }
        
        if lbl_enter_months.text!.count < 1{
         alert_view.removeFromSuperview()
        showToast(controller: self, message: "Issue Type can't be empty", seconds: 0.5)
            return
        }
       let base_url = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let url = base_url + FeedBack
        
        //dlfkdlkfl Optional(["message": New entry is created., "result": 0, "version": 6, "data": <null>])

       // print("SUBMIT PRESSED \(url)")
        
        
        let imageFile = img_feedback.image
        
        let parameters : Dictionary<String, Any> = ["action" : "feedback_submit",
                                                    "app_name" : "GeoTrack",
                                                    "issue_type" : lbl_enter_months.text!,
                                                    "issue_desc" : txtview_description.text!,
                                                    "user_id" : user_id,
                                                    "user_name" : user_name,
                                                    "hash_key" : hash_key
        ]
        
        uploadImageData(inputUrl: url, parameters: parameters, imageName: "image_file", imageFile: imageFile, completion: {data, r_error, isNetwork in
            
            if isNetwork && data != nil{
   
                let result = data!["result"] as! Int
                
                if result == 0{
                   showToast(controller: self, message: "Data Uploaded Successfully.", seconds: 0.5)
                }else{
                     showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
                }
            }else{
                 showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }
    
    @objc func tap_issue_type(){
        txt_issue_type.resignFirstResponder()
        tbl_issue_type.isHidden = false
        
        
    }
    
    @objc func pressed_camera_button(){
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            self .present(picker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }

}

extension FeedBackVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.img_feedback.contentMode = .scaleAspectFit
            self.img_feedback.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}

extension FeedBackVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issue_type.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_issue_type.dequeueReusableCell(withIdentifier: "issue_cell")
        cell?.textLabel?.text = issue_type[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lbl_enter_months.textColor = UIColor.black
        lbl_enter_months.text = issue_type[indexPath.row]
        tbl_issue_type.isHidden = true
    }
    
}


extension FeedBackVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (txtview_description.text == "Type Your Comment")
        {
            txtview_description.text = nil
            txtview_description.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtview_description.text.isEmpty
        {
            txtview_description.text = "Type Your Comment"
            txtview_description.textColor = UIColor.darkGray
        }
        textView.resignFirstResponder()
    }
}
