//
//  RenewLicenseVC.swift
//  GeoTrack
//
//  Created by Georadius on 30/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import Razorpay


class RenewLicenseVC: UIViewController {

    @IBOutlet weak var lbl_select_username: UILabel!
    @IBOutlet weak var super_view_user_name: UIView!
    @IBOutlet weak var tbl_usernames: UITableView!
    @IBOutlet weak var lbl_enter_months: UILabel!
    @IBOutlet weak var btn_drop_down: UIButton!
    @IBOutlet weak var btn_buy: UIButton!
    @IBOutlet weak var view_months: UIView!
    @IBOutlet weak var tbl_drop_down: UITableView!
    @IBOutlet weak var lbl_total_amount: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    @IBOutlet weak var btn_buy_license: UIButton!
    @IBOutlet weak var view_buy_detail: UIView!
    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var search_bar_item: UISearchBar!
    @IBOutlet weak var tbl_license: UITableView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var view_header: UIView!
    var searchActive : Bool = false
    var single_selection_index = [Int]()
     var unit_rate : String!
     var tax_name : String!
    var total_payble_amount_with_tax : String!
 var quote_scheme : String!
     var tax_value : String!
     var filterdData : [RenewLicenseModel] = []
    var data : [RenewLicenseModel] = []
    var view_buy_detail_y : CGFloat = 0.0
    let item_name = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    
    var username = [String]()
    var isSelectAll = false
  //  var isSingleSelection = false
    var userid = [String]()
    let alert_view = AlertView.instanceFromNib()
  var user_id_val = ""
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    var rowsWhichAreChecked = [IndexPath]()
    
    var amount_razorpay : String!
    var requestquote_id : String!
    var payment_id : String!
    var razorpay_paymentId : String!
    var order_id : String!
    var razorpay: Razorpay!
    var razorpayTestKey = ""
    var descriptions : String!
    var name : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_buy_detail.isHidden = true
        view_buy_detail.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        self.view_buy_detail_y = view_buy_detail.frame.origin.y
        super_view_user_name.isHidden = true
        view_months.layer.cornerRadius = 5
        view_months.layer.masksToBounds = true
        view_months.layer.borderColor = UIColor.lightGray.cgColor
        view_months.layer.borderWidth = 0.3
       
        tbl_drop_down.isHidden = true
        tbl_drop_down.delegate = self
        tbl_drop_down.dataSource = self
        
        btn_drop_down.addTarget(self, action: #selector(pressed_drop_down), for: .touchUpInside)
        btn_back.addTarget(self, action: #selector(btn_back_pressed), for: .touchUpInside)
        btn_buy.addTarget(self, action: #selector(pressed_buy_license), for: .touchUpInside)
        btn_filter.addTarget(self, action: #selector(pressed_filter), for: .touchUpInside)
        
        btn_buy_license.addTarget(self, action: #selector(make_payment), for: .touchUpInside)
        
//        tbl_license.delegate = self
//        tbl_license.dataSource = self
      
     
        
        self.tbl_license.register(UINib(nibName: "RenewLicenseCell", bundle: nil), forCellReuseIdentifier: "RenewLicenseCell")

        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tap_months))
        lbl_enter_months.isUserInteractionEnabled = true
        lbl_enter_months.addGestureRecognizer(tapgesture)
        // Do any additional setup after loading the view.
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        let urlString = domain_name + "/billing_api.php?action=billing_details&user_name=" + user_name + "&hash_key=" +  hash_key
        CallDatafromServer(urlString: urlString, isusername: false, getUnitRate: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        lbl_select_username.text = user_name
         view_buy_detail.frame.origin.y = self.view.frame.size.height
        
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        //https://track.gpsplatform.in/vehicle_result.php?action=search_json&user_id=3211&user_name=maaztracking@gmail.com&hash_key=TVPAJOYZ
        
        
        
        let urlString = domain_name + "/vehicle_result.php?action=search_json&user_id=&user_name="  + user_name + "&hash_key=" + hash_key
        CallDatafromServer(urlString: urlString, isusername: false, getUnitRate: false)
    }
    
    @IBAction func btn_cross(_ sender: Any) {
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
   
        
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
    
        let urlString = domain_name + "/vehicle_result.php?action=search_json&user_id=" + user_id_val + "&user_name="  + user_name + "&hash_key=" + hash_key
        CallDatafromServer(urlString: urlString, isusername: false, getUnitRate: false)
        super_view_user_name.isHidden = true
    }
    
    @objc func btn_back_pressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func make_payment(){
        
        if lbl_enter_months.text!.count < 1 || selected_devie_id.count < 1{
            
            return
        }
        
        if self.quote_scheme == "1"{
            SendDataToServer()
            return
        }else if self.quote_scheme == "2"{
            
              CallDataForPayMent()
            return
        }
      
    }
    
    @objc func pressed_buy_license(){
      
        
        let unit = Double(selected_devie_id.count)

        let month = Double(lbl_enter_months.text!) ?? 0

        let amount = Double(self.unit_rate!) ?? 0

        let total_amount = (unit * amount) * month

        // lbl_total_amount.text = "₹ " + String(total_amount)
        

      let val = GetAmountsWithTax(total_amount: total_amount, tax_value: self.tax_value, tax_name: self.tax_name)



        lbl_total_amount.text = val[0]

        self.lbl_amount.text = val[1]

        self.total_payble_amount_with_tax = val[2]
        
        
       
        if self.view_buy_detail.frame.origin.y == self.view_buy_detail_y{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_buy_detail.frame.origin.y = self.view.frame.size.height
                self.view_buy_detail.isHidden = true
            })
        }else{
            view_buy_detail.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view_buy_detail.frame.origin.y = self.view_buy_detail_y
                
            })
        }
    }
    
    @objc func pressed_drop_down(){
        UIView.animate(withDuration: 0.5, animations: {
            self.view_buy_detail.frame.origin.y = self.view.frame.size.height
           
        })
    }
    
    @objc func pressed_filter(){
        super_view_user_name.isHidden = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        let urlString = domain_name + "/billing_api.php?action=renew_licence_details&user_name="  + user_name + "&hash_key=" + hash_key
        CallDatafromServer(urlString: urlString, isusername: true, getUnitRate: false)
    }
    
    @objc func tap_months(){
        tbl_drop_down.reloadData()
        tbl_drop_down.isHidden = false
    }
    
    
    func SendDataToServer(){
        
        self.view.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString_0 = domain_name + "/vehicle_quota_result.php?action=edit_device_quota&device_count=" + String(selected_devie_id.count)
        
        let urlString_1 = urlString_0 + "&billing_term=1&device_ids=" + selected_devie_id.joined(separator:",") + "&billing_term=" + lbl_enter_months.text!
        
        let urlString = urlString_1 + "&user_name=" + user_name +  "&hash_key=" + hash_key
        
        //https://track.gpsplatform.in/vehicle_quota_result.php?action=edit_device_quota&device_count=1&billing_term=1&device_ids=  &user_name=demo@example.com&hash_key=LQCLFDSE
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        //  print("sdfkdkfjdk \(String(describing: encodedString))")
        
        CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                // print("kjkjkjk \(data!)")
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                //print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallDatafromServer(urlString: String, isusername: Bool, getUnitRate: Bool){
      
        if isusername{
            self.userid.removeAll()
            self.username.removeAll()
        }else{
             self.data.removeAll()
        }
       
       
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
       
        
        self.view.addSubview(alert_view)
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
       
        CallShareDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if data != nil{
                
                if getUnitRate{
                    self.unit_rate = data!["unit_rate"] as? String
                    
                    
                    self.tax_name = GetTaxName(Vehicals: data!)
                    //data!["tax_name"] as? String
                    self.tax_value = GetTaxValue(Vehicals: data!)
                    //data!["tax_value"] as? String
                    self.quote_scheme = data!["quota_scheme"] as? String
                    
                    if self.tax_value == "0"{
                        self.lbl_amount.text = "Amount Payble:   " + "₹0.0 " + self.tax_name + "(₹0)"
                        
                    }else{
                        self.lbl_amount.text = "Amount Payble:   " + "₹0.0+" + self.tax_name + "@" + self.tax_value + "% (₹0)"
                        
                    }
                    
                }else{
                    if isusername{
                        let user_group = data!["user_group_search"] as! Array<Any>
                        for val in user_group{
                            let val_data = val as! Dictionary<String, Any>
                            self.username.append(val_data["username"] as! String)
                            self.userid.append(val_data["userid"] as! String)
                        }
                        self.tbl_usernames.delegate = self
                        self.tbl_usernames.dataSource = self
                        self.tbl_usernames.reloadData()
                    }else{
                        let all_data = data!["return_json"] as! Array<Any>
                        for val_data in all_data{
                            let val = val_data as! Dictionary<String, Any>
                            let registration_no = val["registration_no"] as! String
                            let device_serial = val["device_serial"] as! String
                            let valid_date = val["date_display"] as! String
                            let device_id = val["device_id"] as! String
                            
                            self.all_selected_device_id.append(val["device_id"] as! String)
                            let newData = RenewLicenseModel(registration_no: registration_no, device_serial: device_serial, valid_date: valid_date, device_id: device_id)
                            self.data.append(newData)
                            
                        }
                        self.tbl_license.isHidden = false
                        self.tbl_license.delegate = self
                        self.tbl_license.dataSource = self
                        self.tbl_license.reloadData()
                        self.search_bar_item.delegate = self
                        self.filterdData = self.data
                }
                
             
                }
                 self.alert_view.removeFromSuperview()
               return
                
               
            }else{
                if isusername{
                    self.tbl_usernames.isHidden = true
                }else{
                    self.tbl_license.isHidden = true
                }
               // showToast(controller: self, message : "No Record Found", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                
                //showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }

    func CallDataForPayMent(){
        self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            alert_view.removeFromSuperview()
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            return
        }
        
        
        let base_url = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let base_url2 = UserDefaults.standard.value(forKey: "Domain_s") as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
      
        let url = base_url + "/billing_api.php"

        
        let productInfo_1 = "ADD" + String(selected_devie_id.count)
        let productInfo_2 = " VTS Licences for" +  lbl_enter_months.text!
        let productInfo = productInfo_1 + productInfo_2 + " Month"
        

        
//
        let parameters : [String : Any] = ["licence_quantity" : selected_devie_id.count,
                                           "billing_term" : lbl_enter_months.text!,
                                           "domain_name" : base_url2,
                                           "firstname" : user_name,
                                           "action" : "payment_order",
                                           "amount" : total_payble_amount_with_tax!,
                                           "productinfo" : productInfo,
                                           "email" : "",
                                           "phone" : "",
                                           "user_name" : user_name,
                                           "hash_key" : hash_key,
                                           "device_ids" : selected_devie_id.joined(separator:",")
        ]
        
    
        CallApiWithParameters(url: url, parameters: parameters, completion: {data, r_error, isNetwork in
            
            if isNetwork && data != nil{
                let val_data = data!.convertToDictionary()
                let result = val_data![K_Result] as! Int
               // print("skldklk \(String(describing: val_data))")
                
                if result == 0{
                    let all_data = val_data![K_Data] as! Dictionary<String, Any>
                   
                    self.amount_razorpay = String(all_data[AMOUNT] as! Int)
                    self.razorpayTestKey = all_data[KEY] as! String
                    self.descriptions = all_data[DESCRIPTION] as? String
                    self.name = all_data[NAME] as? String
                    self.requestquote_id = all_data[REQUESTQUOTE_ID] as? String
                    self.payment_id = all_data[PAYMENT_ID] as? String
                    self.order_id = all_data[ORDER_ID] as? String
                    self.razorpay = Razorpay.initWithKey(self.razorpayTestKey, andDelegate: self)
                    
                    self.showPaymentForm()
                }else{
                    showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
                }
            }else{
                showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallDataOfOrderSuccess(){
        self.view.addSubview(alert_view)
        if !NetworkAvailability.isConnectedToNetwork() {
            alert_view.removeFromSuperview()
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            return
        }
        
        
        
        let base_url = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        
        let url = base_url + "/billing_api.php"
        
        
        let parameters : [String : Any] = [ "action" : "order_success",
                                            "amount": total_payble_amount_with_tax!,
                                            "licence_quantity" : selected_devie_id.count,
                                            "billing_term" : lbl_enter_months.text!,
                                            "user_name" : user_name,
                                            "hash_key" : hash_key,
                                            "razorpay_paymentId" :  self.razorpay_paymentId!,
                                            "requestquota_id" : requestquote_id!,
                                            "device_ids" : "",
                                            "payment_id" : payment_id!
        ]
        
        CallApiWithParameters(url: url, parameters: parameters, completion: {data, r_error, isNetwork in
            
            if isNetwork && data != nil{
                let val_data = data!.convertToDictionary()
                
                let message = val_data!["message"] as? String
                showToast(controller: self, message: message!, seconds: 1.5)
                
            }else{
                showToast(controller: self, message: "Something went wrong.", seconds: 0.5)
            }
            
            self.alert_view.removeFromSuperview()
        })
        
    }
    
    internal func showPaymentForm(){
        let options: [String:Any] = [
            "amount": amount_razorpay!, //This is in currency subunits. 100 = 100 paise= INR 1.
            "currency": "INR",//We support more that 92 international currencies.
            "description": self.descriptions,
            //"image": "https://url-to-image.png",
            "order_id" : self.order_id!,
            "name": self.name,
            "prefill": [
                "contact": "",
                "email": ""
            ],
            "theme": [
                "color": "#F37254"
            ]
        ]
        razorpay.open(options)
    }
    

}

extension RenewLicenseVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
              return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int = 0
        if tableView == tbl_drop_down{
             count = 12
        }
        
        if tableView == tbl_license{
            
            if(searchActive) {
                count = filterdData.count
            } else {
                count = data.count
            }
               
        }
        
        if tableView == tbl_usernames{
            count = self.username.count
        }
      
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        
        if tableView == tbl_drop_down{
            let cell = tbl_drop_down.dequeueReusableCell(withIdentifier: "num_cell")
            cell?.textLabel?.text = item_name[indexPath.row]
            cellToReturn = cell!
        }
         if tableView == tbl_license{
            let renew_cell = tbl_license.dequeueReusableCell(withIdentifier: "RenewLicenseCell", for: indexPath) as! RenewLicenseCell
                for val in single_selection_index{
                    if indexPath.row == val{
                        if renew_cell.btn_uncheck.isHidden{
                                renew_cell.btn_uncheck.isHidden = false
                            }else{
                             renew_cell.btn_uncheck.isHidden = true
                            
                        }
                    }
                }
            if searchActive{
                
                renew_cell.lbl_registration.text = filterdData[indexPath.row].registration_no
                renew_cell.lbl_valid_date.text = filterdData[indexPath.row].valid_date
                renew_cell.lbl_device_serial.text = filterdData[indexPath.row].device_serial
               
            }else{
                
                renew_cell.lbl_registration.text = data[indexPath.row].registration_no
                renew_cell.lbl_valid_date.text = data[indexPath.row].valid_date
                renew_cell.lbl_device_serial.text = data[indexPath.row].device_serial
                
            }
           
            cellToReturn = renew_cell
        }
        if tableView == tbl_usernames{
            let cell = tbl_usernames.dequeueReusableCell(withIdentifier: "user_cell")
            cell?.textLabel?.text = self.username[indexPath.row]
            cellToReturn = cell!
        }
        
        return cellToReturn
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     

        if tableView == tbl_drop_down{
        lbl_enter_months.textColor = UIColor.black
        lbl_enter_months.text = item_name[indexPath.row]
            
           CalculateAllAmountAndSet()
            
            
        tbl_drop_down.isHidden = true
        }
        
        if tableView == tbl_usernames{
            lbl_select_username.text = self.username[indexPath.row]
            self.user_id_val = self.userid[indexPath.row]
        }
        
        if tableView == tbl_license{
               let cell = tbl_license.cellForRow(at: indexPath) as! RenewLicenseCell
         
            isSelectAll = false
       
            if cell.btn_uncheck.isHidden{
                single_selection_index = single_selection_index.filter(){$0 != indexPath.row}
                self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.row]}

            }else{
                self.selected_devie_id.append(self.all_selected_device_id[indexPath.row])
                single_selection_index.append(indexPath.row)
            }
            
          CalculateAllAmountAndSet()
     
            tbl_license.reloadData()
        }
    
       
    }
    
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tbl_drop_down{
            return tbl_drop_down.frame.size.height / 2
        }else if tableView == tbl_usernames{
         return tbl_usernames.frame.size.height / 8
        }else{
            return tbl_license.frame.size.height / 4
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tbl_drop_down || tableView == tbl_usernames{
            return 0
        }else{
            return 55
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SelectAllView.instanceFromNib()
        if isSelectAll{
            view.btn_select.setImage(UIImage(named: "check"), for: .normal)
        }else{
            view.btn_select.setImage(UIImage(named: "uncheck"), for: .normal)

        }
        view.btn_select.addTarget(self, action: #selector(pressed_select_all), for: .touchUpInside)
        return view
    }
}

extension RenewLicenseVC{
    
    
    func CalculateAllAmountAndSet(){
        let unit = Double(selected_devie_id.count)
        
        let month = Double(lbl_enter_months.text!) ?? 0
        
        let amount = Double(self.unit_rate!) ?? 0
        
        let total_amount = (unit * amount) * month
        
        let val = GetAmountsWithTax(total_amount: total_amount, tax_value: self.tax_value, tax_name: self.tax_name)
        
        lbl_total_amount.text = val[0]
        self.lbl_amount.text = val[1]
        self.total_payble_amount_with_tax = val[2]
    }


    @objc func pressed_select_all(){
      
        selected_devie_id.removeAll()
      single_selection_index.removeAll()
        if isSelectAll{
            isSelectAll = false
            single_selection_index.removeAll()
             selected_devie_id.removeAll()
        }else{
            for (index,_) in data.enumerated(){
          
                single_selection_index.append(index)
                selected_devie_id.append(all_selected_device_id[index])
            }
            isSelectAll = true
        }
   CalculateAllAmountAndSet()
        tbl_license.reloadData()
    }
}


extension RenewLicenseVC: UISearchBarDelegate{

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar_item.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar_item.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar_item.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterdData = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        
        if(filterdData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tbl_license.reloadData()
    }
}
extension RenewLicenseVC : RazorpayPaymentCompletionProtocol{
    public func onPaymentError(_ code: Int32, description str: String){
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    public func onPaymentSuccess(_ payment_id: String){
        
        self.razorpay_paymentId = payment_id
        
        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        CallDataOfOrderSuccess()
    }
    
    
}
