//
//  StatementVC.swift
//  GeoTrack
//
//  Created by Georadius on 30/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class StatementVC: UIViewController {

    @IBOutlet weak var tbl_statement: UITableView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_end_date: UITextField!
    @IBOutlet weak var txt_start_date: UITextField!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var view_header: UIView!
    var isStartDate : Bool!
    let date_picker_super_view = DatePickerView.instanceFromNib()
    let alert_view = AlertView.instanceFromNib()
    var statement_data : [InvoiceModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_submit.addTarget(self, action: #selector(pressed_submit), for: .touchUpInside)
        self.tbl_statement.register(UINib(nibName: "InvoiceCell", bundle: nil), forCellReuseIdentifier: "InvoiceCell")

        btn_back.addTarget(self, action: #selector(btn_back_pressed), for: .touchUpInside)
        txt_end_date.addTarget(self, action: #selector(tap_end_date), for: .editingDidBegin)
        txt_start_date.addTarget(self, action: #selector(tap_start_date), for: .editingDidBegin)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        date_picker_super_view.date_view_picker.addTarget(self, action: #selector(datePickerValueChannged(sender:)), for: .valueChanged)
        date_picker_super_view.btn_cancel.addTarget(self, action: #selector(pressed_cancel), for: .touchUpInside)
        date_picker_super_view.btn_cancel.frame.origin.x = self.view.frame.size.width - 50
        
        
        let date = Date()
        let formetter = DateFormatter()
        formetter.dateFormat = "yyyy-MM-dd"
        date_picker_super_view.date_view_picker.maximumDate = date
        alert_view.frame.size = self.view.frame.size
        
        
        let result = formetter.string(from: date)
        txt_start_date.text = result
        
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        //let formetter = DateFormatter()
        formetter.dateFormat = "yyyy-MM-dd"
        let resultY = formetter.string(from: yesterday!)
        txt_end_date.text = resultY
    }
    
    @objc func btn_back_pressed(){
        self.navigationController?.popViewController(animated: true)
    }
  
    func CallDataFromServer(){
        
        self.view.addSubview(alert_view)
        self.statement_data.removeAll()
        if txt_start_date.text!.count < 1 || txt_end_date.text!.count < 1{
            showToast(controller: self, message: "Start Date & End Date can't be Empty.", seconds: 1.5)
            alert_view.removeFromSuperview()
            return
        }
        
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        //"2622"
            //
        
        let start_date = txt_start_date.text!
        let end_date = txt_end_date.text!
        
        
        
        let urlString = domain_name + "/billing_api.php?action=statement_generate_app&user_id=" + user_id +  "&date_from=" + start_date + "&date_to=" + end_date + "&time_picker_from=00:00:00&time_picker_to=23:59:59&data_format=1&rand=1799849669&user_name=" + user_name + "&hash_key=" + hash_key
        
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
       // print("sdkldkjk \(String(describing: encodedString))")
        
        CallNotificatioSettingDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    let note = val_data["note"] as! String
                    let date = val_data["statement_date"] as! String
                    let invoice_status = val_data["invoice_status"] as! String
                    let total_amount = val_data["total_amount"] as! String
                    
                    let newDetail = InvoiceModel(date: date, invoice_status: invoice_status, note: note, total_amount: total_amount)
                    self.statement_data.append(newDetail)
                    
                }
                self.tbl_statement.isHidden = false
                self.tbl_statement.delegate = self
                self.tbl_statement.dataSource = self
                self.tbl_statement.reloadData()
                
            }else{
                self.tbl_statement.isHidden = true
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
        
        
    }
    

}

extension StatementVC{
    
    @objc func tap_start_date(){
        txt_start_date.resignFirstResponder()
        isStartDate = true
        self.view.addSubview(date_picker_super_view)
        
        return
    }
    
    @objc func tap_end_date(){
        txt_end_date.resignFirstResponder()
        isStartDate = false
        self.view.addSubview(date_picker_super_view)
        print("You tap on End Date")
        return
    }
    
    @objc func pressed_submit(){
        CallDataFromServer()
        return
    }
    
    @objc func pressed_cancel(){
        date_picker_super_view.removeFromSuperview()
    }
    
    @objc func datePickerValueChannged(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if isStartDate{
            txt_start_date.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_end_date.text = dateFormatter.string(from: sender.date)
        }
        
    }
    
}

extension StatementVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statement_data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_statement.dequeueReusableCell(withIdentifier: "InvoiceCell") as! InvoiceCell
        cell.lbl_data.text = self.statement_data[indexPath.row].note
        cell.lbl_date.text = self.statement_data[indexPath.row].date
        cell.lbl_rupee.text = "₹ " + self.statement_data[indexPath.row].total_amount
        
        let status = self.statement_data[indexPath.row].invoice_status
        if status == "0"{
            cell.lbl_success.text = "Success"
            cell.lbl_success.textColor = Moving_Color
        }else if status == "1"{
            cell.lbl_success.text = "Failure"
            cell.lbl_success.textColor = Stopped_Color
        }else{
            cell.lbl_success.text = "In Progress"
            cell.lbl_success.textColor = Global_Yellow_Color
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_statement.frame.size.height / 5
    }
    
}
