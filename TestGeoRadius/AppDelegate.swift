//
//  AppDelegate.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //        <#code#>
    //    }
    
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
    //        <#code#>
    //    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //Check if the app is not in the foreground right now
        if(UIApplication.shared.applicationState != .active) {
            //              let deliveryId = userInfo["_dId"] as? String
            //              let broadlogId = userInfo["_mId"] as? String
            //  print("ldkskdjkjs \(userInfo)")
            UserDefaults.standard.set(true, forKey: "IsNotification")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let sw = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            
            self.window?.rootViewController = sw
            
            let cv = storyboard.instantiateViewController(withIdentifier: "CheckLoginVC") as! CheckLoginVC
            let navigationController = UINavigationController(rootViewController: cv)
            navigationController.setNavigationBarHidden(true, animated: false)
            sw.pushFrontViewController(navigationController, animated: true)
        
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map{ String(format: "%02.2hhx", $0)}.joined()
        //print("Token : \(token)")
        UserDefaults.standard.set(token, forKey: "DEVICE_TOKEN")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error Notification : \(error)")
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Thread.sleep(forTimeInterval: 0.5)
        UserDefaults.standard.set(false, forKey: "IsNotification")
        if UserDefaults.standard.value(forKey: LoginKey) == nil{
            SaveLoginKey(key: "")
        }
        
        GMSServices.provideAPIKey("AIzaSyBhlhs52SR0drBuhvDykz7_HED9LkGwabA")
        GMSPlacesClient.provideAPIKey("AIzaSyBhlhs52SR0drBuhvDykz7_HED9LkGwabA")
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {
            (granted, error) in
            print("Granted: \(granted)")
        })
        
        UIApplication.shared.registerForRemoteNotifications()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}



