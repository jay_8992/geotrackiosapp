//
//  VehicleCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleCell: UITableViewCell {

    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var img_check: UIImageView!
    @IBOutlet weak var img_uncheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
      
        self.img_check.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
