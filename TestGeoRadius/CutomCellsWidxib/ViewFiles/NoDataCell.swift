//
//  NoDataCell.swift
//  GeoTrack
//
//  Created by Georadius on 25/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class NoDataCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
