//
//  Vehicle_Detail_Cell.swift
//  TestGeoRadius
//
//  Created by Georadius on 04/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit



class Vehicle_Detail_Cell: UITableViewCell {
    @IBOutlet weak var lbl_registration: UILabel!
    @IBOutlet weak var btn_down: UIButton!
    @IBOutlet weak var speed_picker: UIPickerView!
    @IBOutlet weak var btn_play_pause: UIButton!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_date_time: UILabel!
    
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code\
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

