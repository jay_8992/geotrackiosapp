//
//  AlertView.swift
//  TestGeoRadius
//
//  Created by Georadius on 25/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class AlertView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AlertView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
