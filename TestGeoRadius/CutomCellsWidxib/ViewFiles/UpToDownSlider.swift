//
//  UpToDownSlider.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation
import UIKit



func SetViewOuter(subView: UIView){
    subView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    subView.layer.shadowColor = UIColor.black.cgColor
    subView.layer.shadowOpacity = 0.1
    subView.layer.shadowRadius = 10
    subView.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 3), radius: 5, scale: true)
}

func LayoutSubView(subView: UIView, mainView: UIView){
    
    var bottomConstraint = NSLayoutConstraint()
    let popupOffset: CGFloat = 440
    
    subView.translatesAutoresizingMaskIntoConstraints = false
    subView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
    subView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
    bottomConstraint = subView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: popupOffset)
    bottomConstraint.isActive = true
    subView.heightAnchor.constraint(equalToConstant: 500).isActive = true
}


