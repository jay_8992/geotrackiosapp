//
//  NotificationSettingCell.swift
//  GeoTrack
//
//  Created by Georadius on 14/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class NotificationSettingCell: UITableViewCell {

    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_notification_name: UILabel!
    @IBOutlet weak var switch_setting: UISwitch!
    @IBOutlet var lbl_days: [UILabel]!
    @IBOutlet weak var view_status: UIView!
    @IBOutlet weak var view_content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
       
        SetDaysLableSize()
        
        // Initialization code
    }

    func SetDaysLableSize(){
        view_status.frame.size.width = view_status.frame.size.height
        view_status.layer.cornerRadius = view_status.frame.size.height / 7
        view_status.layer.borderColor = UIColor.black.cgColor
        view_status.layer.borderWidth = 0.5
    
        switch_setting.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        for index in 0...6{
            lbl_days[index].frame.size.width = lbl_days[index].frame.size.height
            lbl_days[index].layer.cornerRadius = 10
//            lbl_days[index].layer.borderColor = UIColor.black.cgColor
//            lbl_days[index].layer.borderWidth = 0.5
            lbl_days[index].dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        }
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
