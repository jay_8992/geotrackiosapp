//
//  RenewLicenseCell.swift
//  GeoTrack
//
//  Created by Georadius on 03/08/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class RenewLicenseCell: UITableViewCell {
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var lbl_registration: UILabel!
    @IBOutlet weak var lbl_device_serial: UILabel!
    @IBOutlet weak var lbl_valid_date: UILabel!
    @IBOutlet weak var btn_uncheck: UIButton!
    
    @IBOutlet weak var btn_check: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        view_background.layer.cornerRadius = view_background.frame.size.height / 18
        view_background.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
       
            btn_uncheck.isHidden = false
       
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
