//
//  Date_and_Time.swift
//  TestGeoRadius
//
//  Created by Georadius on 04/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


class Date_and_Time: UITableViewCell {

    @IBOutlet weak var txt_to_date_time: UITextField!
    @IBOutlet weak var txt_from_date_time: UITextField!
    
    @IBOutlet weak var view_content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txt_from_date_time.text = TodayFromDate()
        txt_to_date_time.text = TodayToDate()
   
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
