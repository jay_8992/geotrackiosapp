//
//  Vehicle_Data_Cell.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class Vehicle_Data_Cell: UITableViewCell {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var img_heading: UIImageView!
    
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_data: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
