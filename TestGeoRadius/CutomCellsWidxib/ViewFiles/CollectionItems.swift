//
//  CollectionItems.swift
//  TestGeoRadius
//
//  Created by Georadius on 26/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class CollectionItems: UICollectionViewCell {
    
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
}
