//
//  DashboardItemCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 05/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class DashboardItemCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var lbl_alert: UILabel!
    @IBOutlet weak var view_item: UIView!
}
