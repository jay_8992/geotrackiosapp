//
//  UserAddCell.swift
//  GeoTrack
//
//  Created by Georadius on 17/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UserAddCell: UITableViewCell {

    @IBOutlet weak var view_content: UIView!
    
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    
    @IBOutlet weak var lbl_user_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
              view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        view_content.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
