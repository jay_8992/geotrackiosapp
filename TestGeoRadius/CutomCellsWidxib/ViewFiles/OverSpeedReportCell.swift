//
//  OverSpeedReportCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class OverSpeedReportCell: UITableViewCell {
    @IBOutlet weak var lbl_location: UILabel!
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_speed: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
