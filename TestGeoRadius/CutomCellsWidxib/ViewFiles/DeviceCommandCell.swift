//
//  DeviceCommandCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 22/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class DeviceCommandCell: UITableViewCell {

    @IBOutlet weak var lbl_command: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_response: UILabel!
    
    @IBOutlet weak var view_content: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
