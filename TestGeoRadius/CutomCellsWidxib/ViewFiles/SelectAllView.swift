//
//  SelectAllView.swift
//  GeoTrack
//
//  Created by Georadius on 03/08/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class SelectAllView: UIView {

    @IBOutlet weak var btn_select: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> SelectAllView {
        let myClassNib = UINib(nibName: "SelectAllView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! SelectAllView
    }


}
