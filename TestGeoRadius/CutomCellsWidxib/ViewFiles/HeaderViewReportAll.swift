//
//  HeaderViewReportAll.swift
//  TestGeoRadius
//
//  Created by Georadius on 17/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class HeaderViewReportAll: UIView {

    @IBOutlet weak var lbl_registration: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
//    class func instanceFromNib() -> UIView {
//        return UINib(nibName: "HeaderViewReportAll", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
//    }
    class func instanceFromNib() -> HeaderViewReportAll {
        let myClassNib = UINib(nibName: "HeaderViewReportAll", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! HeaderViewReportAll
    }

}
